'''
    views.py
    
    Defines the application logic as a GenTL Consumer. Currently TL is a module
    global and will never fall out of scope or be closed. Interfaces are opened
    with start_scan() and never closed. 
    
    Since TL is already a global, it is a decent enough place to store our
    known camera list and active camera list. 
'''

# stdlib
import sys
import os
import base64
import cProfile

def profiled(func):
    def wrapper(*args, **kwargs):
        datafn = func.__name__ + ".profile" # Name the data file sensibly
        prof = cProfile.Profile()
        retval = prof.runcall(func, *args, **kwargs)
        prof.dump_stats(datafn)
        return retval

    return wrapper


# flask
from flask import url_for, render_template, session, request, redirect, jsonify, send_from_directory

# our resources
from resources import app, TL
from camera import Camera

class DBInfo(object):
    def __init__(self, addr, port, name, collection):
        self.addr = addr
        self.port = port
        self.name = name
        self.collection = collection
        
DB_INFO = None #DBInfo('192.168.0.145', 27017, 'test', 'test')

###############################################################################
## Device Tracking 
## every opened GenTL Device becomes a Camera in open_cameras
## every seen but unopened GenTL device is in known_cameras
open_cameras = []
known_cameras = []

def open_camera(id):
    print 'opening camera id {}'.format(id)
    for kc in known_cameras:
        if kc.id == id:
            kc.open()
            open_cameras.append(kc)
            known_cameras.remove(kc)
            
def get_camera(id):
    for oc in open_cameras:
        if oc.id == id:
            return oc
    else:
        raise Exception('Could not find camera by ID')

###############################################################################
##  URIs for static html
##  
@app.route('/')
def camera_index():
    ''' 
    one page app, contains all of the possible views
    '''
    return send_from_directory('templates', 'camera.html')
    
@app.route('/help')
def help():
    '''
    help document
    '''
    return 'OK'
    
###############################################################################
##  Commands
##  
#@profiled
def device_scan():
    ''' 
        open every interface and scan for devices on the interface. 
        check and update open_cameras and known_cameras
        
        POST form requires:
            timeout: integer in milliseconds
    '''
    timeout = int(request.form['timeout'])
    TL.update_interface_list(0)
    current_cameras = []
    for iface in TL.interfaces:
        print 'device scan checking IF {} .handle = {}'.format(iface.id, iface._handle)
        if not iface.is_open():
            iface.open()
        iface.update_device_list(timeout / len(TL.interfaces))
        current_cameras.extend([Camera(iface, d, db_info=DB_INFO) for d in iface.devices])
        
    # check to see if any cameras fell out of existence
    open_cameras = [oc for oc in open_cameras 
                            if (oc.id in [d.id for d in current_cameras])]
    
    # all non open and existing cameras
    known_cameras = [cd for cd in current_cameras 
                            if not (cd.id in [oc.id for oc in open_cameras])]
    return 'OK'
    
@app.route('/device/scan', methods=['POST'])
def start_device_scan():
    return device_scan()
    
@app.route('/camera_start', methods=['POST'])
def camera_start():
    '''
        POST form requires:
            camera: string camera id
            mode: 'snapshot' OR 'continuous'
            type: 'imaging' OR 'powermeter'
            
            if 'imaging':
                'quality': integer 0-100
                
            if 'powermeter':
                'sample_count': integer, any power of two between 2^6 and 2^16
                'modulation': 'static bmp' or 'all on' or 'half on'
    '''
    camera = get_camera()
    
    mode = str(request.form['mode'])
    if mode == 'snapshot':
        mode = AcquisitionMode.SingleFrame
    elif mode == 'continuous':
        mode = AcquisitionMode.Continuous
    else:
        raise Exception('unknown acquisition mode')
        
    type = str(request.form['type'])
    if type == 'imaging':
        quality = int(request.form['quality'])
        camera.start_imaging(quality)
    elif type == 'power_meter':
        sample_count = int(request.form['sample_count'])
        modulation = str(request.form['modulation'])
        camera.start_sampling(sample_count, modulation)
    else:
        raise Exception('unknown acquisition mode')
        
    
    return 'OK'
    
@app.route('/camera_stop', methods=['POST'])
def camera_stop():
    '''
        POST form requires:
            camera: string camera id
    '''
    id = str(request.form['camera_id'])
    get_camera(id).stop()
    return 'OK'
    
@app.route('/camera_open', methods=['POST'])
def camera_open():
    '''
        POST form requires:
            camera: string camera id
    '''
    id = str(request.form['camera_id'])
    open_camera(id)
    return 'OK'

###############################################################################
##  Status
##      
@app.route('/interfaces')
def get_interfaces():
    ''' the list of currently known interfaces '''
    return jsonify({"interfaces": [iface.id for iface in TL.interfaces]})
    
@app.route('/camera_json')
def get_cameras():
    print "interfaces: " + ','.join([iface.id for iface in TL.interfaces])
    return jsonify({
            "isInitialized"	: len(TL.interfaces) > 0,
            "isCamerasConnected" : len(open_cameras) > 0,
            "open_cameras" : [oc.status() for oc in open_cameras],
            "known_cameras" : [kc.info() for kc in known_cameras],
            "interfaces": [{'id': iface.id} for iface in TL.interfaces]
        })
    

@app.route('/open_cameras')
def open_cameras():
    '''
    view an overview of the cameras with ability to add and remove
    '''
    return jsonify({"open_cameras" : [oc.status() for oc in open_cameras]})
    
@app.route('/known_cameras')
def known_cameras():
    return jsonify({"known_cameras" : [kc.info() for kc in known_cameras]})

@app.route('/help/json')
def get_help():
    return render_template('help.json')
    
