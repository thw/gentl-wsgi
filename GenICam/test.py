﻿#
#    Testing! 
#
import sys, os, copy
import unittest
from ctypes import *

from GenICam import Root
from Interface import Interface
from Device import Device
from TransportLayer import TransportLayer

from TransportLayerEnums import TLInfoCommand
        
class TransportLayerTest(unittest.TestCase):            
    def setUp(self):
        self.TL = TransportLayer(WinDLL('InView.dll'))
    
    def test_multiple_if_update(self):
        ''' make sure that interfaces arent listed twice after 
        TL.update_interface_list(0) is called. '''
        self.TL.update_interface_list(0)
        self.TL.update_interface_list(0)
        
        iface_ids = [iface.id for iface in self.TL.interfaces]
        print 'interfaces = [{}]'.format(iface_ids)
        self.assertTrue(len(iface_ids) == len(set(iface_ids)))
        
if __name__=='__main__':
    unittest.main()