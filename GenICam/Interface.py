﻿#
#    Interface
#
#    python wrapper to a shared library providing the GenTL 1.2 interface.
#
import sys, os, copy
from ctypes import *

from Device import Device
from TransportLayerEnums import InterfaceInfoCommand
        
class Interface(object):
    ''' 
    Implements GenTL Interface spec
    '''
    def __init__(self, tl, id):
        self._tl = tl
        self.id = id
        self._handle = c_int(0)
        
        self.display_name = self.info(InterfaceInfoCommand.DISPLAY_NAME)
        self.tl_type = self.info(InterfaceInfoCommand.TL_TYPE)
        
        self.devices = []
    
    def is_open(self):
        return self._handle.value != 0
        
    def open(self):
        '''
            open and get an IF_HANDLE. which you shouldnt do twice. 
        '''
        if self.is_open():
            # already open. 
            return
            
        self._tl._check(self._tl.slib.TLOpenInterface(self._tl.tl_id, self.id, byref(self._handle)))
        print 'Interface.open(), handle = {} = {}'.format(self._handle, self._handle.value)
        
    def update_device_list(self, timeout):
        changed = c_byte(0)
        self._tl._check(self._tl.slib.IFUpdateDeviceList(self._handle, byref(changed), c_ulonglong(0)))
        n_devs = c_int(0) 
        self._tl._check(self._tl.slib.IFGetNumDevices(self._handle, byref(n_devs)))
        print 'iface {} sees {} devices'.format(self.id, n_devs.value)
        
        self.devices = []
        for dev_index in range(n_devs.value):        
            dev_id = self._tl._get_string(lambda buf,sz: 
                self._tl.slib.IFGetDeviceID(self._handle, c_int(dev_index), buf, sz))
            self.devices.append(Device(self._tl, dev_id, self._handle))
        
    def info(self, info_type_flag):
        ''' return the requested info '''
        buf = self._tl._get_info_buf(
            lambda buf_type, buf, size: 
                self._tl.slib.TLGetInterfaceInfo(self._tl.tl_id, self.id, 
                                                 info_type_flag, 
                                                 buf_type, buf, size)
        )
        return str(buf)        
        
        
        