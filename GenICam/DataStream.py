﻿#
#    Transport Layer
#
#    python wrapper to a shared library providing the GenTL 1.2 interface.
#    Only create one of these! 
#
import sys, os, copy
from ctypes import *

from GenICam import Root
from Enums import *

import datetime
    
class DataStream(object):    
    '''
    Implements the GenTL DataStream specification.
    '''
    PNG_BASE64 = 'image/png/base64'
    RAW_CH_0 = 'cimage/raw/0'
    CIMAGE_BSON = 'cimage/bson'
    POWERSPECTRUM_CH_0 = 'cimage/raw/0/powerspectrum'
    
    def __init__(self, tl, id, dev_handle):
        self._tl = tl
        self.id = id
        self._handle = c_int(0)
        self.open(dev_handle)
        
    def open(self, dev_handle):
        self._tl._check(self._tl.slib.DevOpenDataStream(dev_handle, self.id, byref(self._handle)))
        
    def start(self):
        acq_flags = c_int(0) #unused
        count = c_longlong(100) #unused
        self._tl._check(self._tl.slib.DSStartAcquisition(self._handle, acq_flags, count))
        
    def stop(self):
        acq_flags = c_int(0) #unused
        self._tl._check(self._tl.slib.DSStopAcquisition(self._handle, acq_flags))
        
    def close(self):
        self._tl._check(self._tl.slib.DSClose(self._handle))
        
    def ready_count(self):
        ''' query DSGetInfo for STREAM_INFO_NUM_AWAIT_DELIVERY (== 5) '''
        flag_num_await = c_int(5) 
        #buf should just be an int
        count = self._tl._get_info_buf(lambda infotype, buf, size: 
            self._tl.slib.DSGetInfo(self._handle, flag_num_await, infotype, buf, size)
        )
        return count
        
    def get_buffer(self):
        '''
        Get a buffer from the datastream, copy the data to python,
        and requeue the buffer in GenTL
        '''
        if self.ready_count() == 0:
            return None
        b = Buffer(self._tl, self._handle)
        return b
        
    def id_key(self):
        ''' translate / to _ in ID for use as a key '''
        return self.id.replace('/', '_')
        