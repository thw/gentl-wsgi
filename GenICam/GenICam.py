##############################################################################
#
# templates/GenICam.py
#
# generated on 2012-05-06 22:25:15.892000
#

import re
import datetime
import copy
import os
import ctypes

def type_or_none(type, something):
    if something is None:
        return something
    else:
        return type(something)     
        
def float_or_none(something):
    return type_or_none(float, something)
    
def int_or_none(something):
    return type_or_none(int, something)
    
def str_or_none(something):
    return type_or_none(str, something)
    
def str_to_bool(s):
    if s is None:
        return s
    if type(s) is bool:
        return s
    return s.lower() in ("y", "yes", "true", "t", "1")    
    
def hex_int(s):
    if s is None:
        return None
    if s[0:2] == '0x':
        return int(s,16)
    raise ValueError('unknown hex-int representation {}'.format(s))    
    
VisibilityBeginner = 'Beginner'
VisibilityLevels = [VisibilityBeginner, 'Expert', 'Guru', 'Invisible']
def get_visibility_levels():
    return VisibilityLevels
    
class Node(object):
    ''' A GenICam Node '''
    def __init__(self):
        self.tag = 'Feature' #tag should only be set in leaf child classes
        self.Name = 'Unnamed'
    def __str__(self):
        return '<Feature {} Name=\"{}\">'.format(self.tag, self.Name)
    def set(self, featDict, otherKeys=[]):        
        keys = [('Name',str),('Description',str),('Visibility',str),('pValue',str),('Value',str)]
        keys.extend(otherKeys)
        for (key,func) in keys:
            if key in featDict and featDict[key] is not None:
                self.__dict__[key] = func(featDict[key])
        
class Feature(Node):
    def __init__(self):
        self.Description = 'No Description'
        self.Visibility = VisibilityBeginner
        self.Value = None
        self.pValue = None
    def set(self, featDict, otherKeys=[]):
        keys = [('Description', str),('Visibility', str),('Value', str),('pValue', str)]
        keys.extend(otherKeys)
        Node.set(self, featDict, keys)
        keys = [('Name',str),('Description',str),('Visibility',str),('pValue',str),('Value',str)]
        for k in otherKeys:
            keys.append(k)
        for (key,func) in keys:
            if key in featDict and featDict[key] is not None:
                self.__dict__[key] = func(featDict[key])
        
# <Representation> Linear, Logarithmic, Boolean (checkbox), PureNumber (only with decimal display), 
# HexNumber, IPV4Address, MACAddress 
class Number(Feature):
    def __init__(self):
        Feature.__init__(self)
        self.Representation = None # string 
        self.Unit = None # string
    def set(self, featDict, otherKeys=[]):
        keys = [('Unit', str),('Representation', str)]
        keys.extend(otherKeys)
        Feature.set(self, featDict, keys)
        
class Integer(Number):
    def __init__(self):
        Number.__init__(self)
        self.tag = 'Integer'
        self.Min = None # int
        self.Max = None # int
        self.Inc = None # int
    def set(self, featDict, otherKeys=[]):
        keys = [('Min', int),
                ('Max', int),
                ('Inc', int),
                ('DisplayNotation', str),
                ('DisplayPrecision', int)]
        keys.extend(otherKeys)
        Number.set(self, featDict, keys)
        
class Float(Number):
    def __init__(self):
        Number.__init__(self)
        self.tag = 'Float'
        self.Min = None # float
        self.Max = None # float
        self.Inc = None # float
        self.DisplayNotation = None # str Fixed, Automatic, Scientific
        self.DisplayPrecision = None # int (0..64)
    def set(self, featDict, otherKeys=[]):
        keys = [('Min', float),
                ('Max', float),
                ('Inc', float),
                ('DisplayNotation', str),
                ('DisplayPrecision', int)]
        keys.extend(otherKeys)
        Number.set(self, featDict, keys)

class Command(Feature):
    def __init__(self):
        Feature.__init__(self)
        self.tag = 'Command'
        self.CommandValue = None
    def set(self, featDict):
        keys = [('CommandValue', int)]
        Feature.set(self, featDict, keys)
        
class EnumEntry(object):
    def __init__(self, n, v):
        self.Name = n
        self.Value = v       
        
class Enumeration(Feature):
    def __init__(self):
        Feature.__init__(self)
        self.tag = 'Enumeration'
        self.enumEntries = []
        
    def addEnumEntry(self,ee):
        enumEntry = EnumEntry(str(ee[0]), int(ee[1]))
        self.enumEntries.append(enumEntry)
    
    def getEnumEntry(self, enumName):
        for ee in self.enumEntries:
            if ee.Name == enumName:
                return ee
        return None
            
    def set(self, featDict):
        keys = []
        newEntries = featDict['enumEntries']
        for ne in newEntries:
            self.enumEntries.append(EnumEntry(ne['Name'], ne['Value']))
        Feature.set(self, featDict, keys)
        
class IntSwissKnife(Feature):
    def __init__(self):
        Feature.__init__(self)
        self.tag = 'IntSwissKnife'
        self.formula = None
        
class SwissKnife(Feature):
    def __init__(self):
        Feature.__init__(self)
        self.tag = 'SwissKnife'
        self.formula = None
        
def get_access_modes():
    return ['RW', 'RO', 'WO', 'NA']
def get_cache_modes():
    return ['NoCache', 'WriteThrough', 'WriteAround']
    
class Register(Node):
    def __init__(self):
        Node.__init__(self)
        self.tag = 'Register'
        self.Address = None   
        self.Length = 4
        self.AccessMode = 'RO'
        self.pPort = None
        self.Cachable = None
        self.PollingTime = None
        self.Endianness = None 
    def set(self, featDict, otherKeys=[]):
        keys = [('Address', int),
                ('Length', int),
                ('AccessMode', str),
                ('pPort', str),
                ('Cachable', str),
                ('PollingTime', int),
                ('Endianness', str)]
        keys.extend(otherKeys)
        Node.set(self, featDict, keys)
               
class IntReg(Register):
    def __init__(self):
        Register.__init__(self)
        self.tag = 'IntReg'
        self.Signed = False
        self.c_type = ctypes.c_int
        self.param_type = ctypes.byref
        self.py_type = int
    def set(self, featDict, otherKeys=[]):
        keys = [('Signed', str_to_bool)]
        keys.extend(otherKeys)
        Register.set(self, featDict, keys)
    def write(self, tl, value):
        Register.write(tl, ctypes.c_int(value))
        
        
class FloatReg(Register):
    def __init__(self):
        Register.__init__(self)
        self.tag = 'FloatReg'
        self.Length = 8
        self.c_type = ctypes.c_double
        self.param_type = ctypes.byref
        self.py_type = float
        
class StringReg(Register):
    def __init__(self):
        Register.__init__(self) 
        self.tag = 'StringReg'
        self.Length = 160    
        self.py_type = str
        self.param_type = (lambda x: x)
    def c_type(self, py_value):
        buf = ctypes.create_string_buffer(self.Length)
        buf.value = py_value[0:self.Length]
        return buf
    def c_type(self):
        buf = ctypes.create_string_buffer(self.Length)
        return buf
     
class Category(object):
    def __init__(self, name, baseAddr):
        self.features = []
        self.Name = name
        self.baseAddr = baseAddr
        self.regAddr = baseAddr
        
    def add_feature(self, feature):
        self.features.append(feature)
                    
    def __str__(self):
        return 'GenICam Category {} with {} features'.format(self.Name, len(self.features))
        
def make_feature(tag):
    # clip off namespace, match to node type
    nsEnd = tag.rfind('}')
    if nsEnd != -1:
        tag = tag[nsEnd+1:]
    for t in [Integer, Float, Command, Enumeration, IntSwissKnife, IntReg, FloatReg, StringReg, SwissKnife]:
        at = t()
        if at.tag==tag:
            #print "tag {} likes feature type {}".format(tag, t)
            return at
    raise Exception('UnknownFeatureTag', tag)
    return None
    
class Root(object):
    def __init__(self):
        self.categories = []
                
        c = Category('DeviceInformation', 0x10000)
        
        f = make_feature('StringReg')
        f.set({'PollingTime': None, 'Cachable': 'WriteThrough', 'AccessMode': 'RO', 'pPort': 'Device', 'Visibility': 'Beginner', 'Length': 32, 'tag': 'StringReg', 'Value': None, 'Endianness': None, 'Address': 72, 'Name': 'DeviceVendorName', 'pValue': None, 'Description': 'This feature provides the name of the manufacturer of the device.'})
        c.add_feature(f)
        
        f = make_feature('StringReg')
        f.set({'PollingTime': None, 'Cachable': 'WriteThrough', 'AccessMode': 'RO', 'pPort': 'Device', 'Visibility': 'Beginner', 'Length': 32, 'tag': 'StringReg', 'Value': None, 'Endianness': None, 'Address': 104, 'Name': 'DeviceModelName', 'pValue': None, 'Description': 'This feature provides the model of the device.'})
        c.add_feature(f)
        
        f = make_feature('StringReg')
        f.set({'PollingTime': None, 'Cachable': 'WriteThrough', 'AccessMode': 'RO', 'pPort': 'Device', 'Visibility': 'Beginner', 'Length': 48, 'tag': 'StringReg', 'Value': None, 'Endianness': None, 'Address': 168, 'Name': 'DeviceManufacturerInfo', 'pValue': None, 'Description': 'This feature provides extended manufacturer information about the device.'})
        c.add_feature(f)
        
        f = make_feature('StringReg')
        f.set({'PollingTime': None, 'Cachable': 'WriteThrough', 'AccessMode': 'RO', 'pPort': 'Device', 'Visibility': 'Beginner', 'Length': 32, 'tag': 'StringReg', 'Value': None, 'Endianness': None, 'Address': 136, 'Name': 'DeviceVersion', 'pValue': None, 'Description': 'This feature provides the version of the device.'})
        c.add_feature(f)
        
        f = make_feature('StringReg')
        f.set({'PollingTime': None, 'Cachable': 'WriteThrough', 'AccessMode': 'RO', 'pPort': 'Device', 'Visibility': 'Beginner', 'Length': 16, 'tag': 'StringReg', 'Value': None, 'Endianness': None, 'Address': 216, 'Name': 'DeviceID', 'pValue': None, 'Description': 'This feature stores a camera identifier.'})
        c.add_feature(f)
        
        f = make_feature('StringReg')
        f.set({'PollingTime': None, 'Cachable': 'WriteThrough', 'AccessMode': 'RW', 'pPort': 'Device', 'Visibility': 'Beginner', 'Length': 16, 'tag': 'StringReg', 'Value': None, 'Endianness': None, 'Address': 232, 'Name': 'DeviceUserID', 'pValue': None, 'Description': 'This feature stores a user-programmable identifier.'})
        c.add_feature(f)
        
        f = make_feature('Command')
        f.set({'Description': 'This command is used to reset the device and to put it in its power up state.', 'Value': None, 'tag': 'Command', 'CommandValue': 0, 'Visibility': 'Beginner', 'pValue': 'DeviceResetReg', 'Name': 'DeviceReset'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': 'WriteThrough', 'AccessMode': 'WO', 'pPort': 'Device', 'Signed': None, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': None, 'Address': 59392, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'DeviceResetReg'})
        c.add_feature(f)
        
        f = make_feature('Integer')
        f.set({'Description': "Returns the camera's serial number.", 'DisplayPrecision': None, 'Min': None, 'Max': None, 'DisplayNotation': None, 'Value': None, 'tag': 'Integer', 'Visibility': 'Beginner', 'Representation': 'Linear', 'Inc': 1, 'pValue': 'DeviceSerialNumberReg', 'Unit': None, 'Name': 'DeviceSerialNumber'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': 'WriteThrough', 'AccessMode': 'RO', 'pPort': 'Device', 'Signed': None, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': None, 'Address': 59396, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'DeviceSerialNumberReg'})
        c.add_feature(f)
        
        f = make_feature('Enumeration')
        f.set({'Description': 'No Description', 'Value': None, 'enumEntries': [{'Name': 'Static', 'Value': 0}, {'Name': 'Active', 'Value': 1}], 'tag': 'Enumeration', 'Visibility': 'Beginner', 'Name': 'DeviceState', 'txt': 'typedef enum {\n        DEVICE_STATE_IDLE = 0,\n        DEVICE_STATE_ACQUIRE_SINGLE = 1,\n        DEVICE_STATE_ACQUIRE_CONTINUOUS = 2,\n        DEVICE_STATE_ERROR = 3,\n        DEVICE_STATE_STOPPING = 4\n    } device_state;\n', 'pValue': 'DeviceStateReg', 'c_name': 'device_state'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': None, 'AccessMode': 'RO', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': None, 'Address': 65536, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'DeviceStateReg'})
        c.add_feature(f)
        
        f = make_feature('Float')
        f.set({'Description': 'No Description', 'DisplayPrecision': None, 'Min': None, 'Max': None, 'DisplayNotation': None, 'Value': None, 'tag': 'Float', 'Visibility': 'Beginner', 'Representation': None, 'Inc': None, 'pValue': 'DeviceVoltageReg', 'Unit': None, 'Name': 'DeviceVoltage'})
        c.add_feature(f)
        
        f = make_feature('FloatReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RO', 'pPort': None, 'Visibility': None, 'Length': 8, 'tag': 'FloatReg', 'Value': None, 'Endianness': 'LittleEndian', 'Address': 65540, 'Name': 'DeviceVoltageReg', 'pValue': None, 'Description': None})
        c.add_feature(f)
        
        f = make_feature('StringReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RO', 'pPort': None, 'Visibility': None, 'Length': 4096, 'tag': 'StringReg', 'Value': None, 'Endianness': 'LittleEndian', 'Address': 65556, 'Name': 'DeviceErrorInfo', 'pValue': None, 'Description': None})
        c.add_feature(f)
        
        f = make_feature('Float')
        f.set({'Description': 'No Description', 'DisplayPrecision': None, 'Min': 0.0, 'Max': 1.0, 'DisplayNotation': 'Fixed', 'Value': None, 'tag': 'Float', 'Visibility': 'Beginner', 'Representation': 'Linear', 'Inc': 0.0001, 'pValue': 'DeviceReconstructProgressReg', 'Unit': None, 'Name': 'DeviceReconstructProgress'})
        c.add_feature(f)
        
        f = make_feature('FloatReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RO', 'pPort': None, 'Visibility': None, 'Length': 8, 'tag': 'FloatReg', 'Value': None, 'Endianness': 'LittleEndian', 'Address': 69652, 'Name': 'DeviceReconstructProgressReg', 'pValue': None, 'Description': None})
        c.add_feature(f)
        
        f = make_feature('Float')
        f.set({'Description': 'No Description', 'DisplayPrecision': None, 'Min': None, 'Max': None, 'DisplayNotation': None, 'Value': None, 'tag': 'Float', 'Visibility': 'Beginner', 'Representation': None, 'Inc': None, 'pValue': 'DeviceAcquireProgressReg', 'Unit': None, 'Name': 'DeviceAcquireProgress'})
        c.add_feature(f)
        
        f = make_feature('FloatReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RO', 'pPort': None, 'Visibility': None, 'Length': 8, 'tag': 'FloatReg', 'Value': None, 'Endianness': 'LittleEndian', 'Address': 69660, 'Name': 'DeviceAcquireProgressReg', 'pValue': None, 'Description': None})
        c.add_feature(f)
        
        self.categories.append(c)
                
        c = Category('ImageSizeControl', 0x20000)
        
        f = make_feature('Integer')
        f.set({'Description': 'No Description', 'DisplayPrecision': None, 'Min': None, 'Max': None, 'DisplayNotation': None, 'Value': None, 'tag': 'Integer', 'Visibility': 'Beginner', 'Representation': None, 'Inc': None, 'pValue': 'PatternScaleXReg', 'Unit': None, 'Name': 'PatternScaleX'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': None, 'AccessMode': 'RO', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': None, 'Address': 196616, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'PatternScaleXReg'})
        c.add_feature(f)
        
        f = make_feature('Integer')
        f.set({'Description': 'No Description', 'DisplayPrecision': None, 'Min': None, 'Max': None, 'DisplayNotation': None, 'Value': None, 'tag': 'Integer', 'Visibility': 'Beginner', 'Representation': None, 'Inc': None, 'pValue': 'PatternScaleYReg', 'Unit': None, 'Name': 'PatternScaleY'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': None, 'AccessMode': 'RO', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': None, 'Address': 196620, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'PatternScaleYReg'})
        c.add_feature(f)
        
        f = make_feature('Integer')
        f.set({'Description': 'No Description', 'DisplayPrecision': None, 'Min': None, 'Max': None, 'DisplayNotation': None, 'Value': None, 'tag': 'Integer', 'Visibility': 'Beginner', 'Representation': None, 'Inc': None, 'pValue': 'ImageSizeXReg', 'Unit': None, 'Name': 'ImageSizeX'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RW', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': 'LittleEndian', 'Address': 196624, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'ImageSizeXReg'})
        c.add_feature(f)
        
        f = make_feature('Integer')
        f.set({'Description': 'No Description', 'DisplayPrecision': None, 'Min': None, 'Max': None, 'DisplayNotation': None, 'Value': None, 'tag': 'Integer', 'Visibility': 'Beginner', 'Representation': None, 'Inc': None, 'pValue': 'ImageSizeYReg', 'Unit': None, 'Name': 'ImageSizeY'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RW', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': 'LittleEndian', 'Address': 196628, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'ImageSizeYReg'})
        c.add_feature(f)
        
        self.categories.append(c)
                
        c = Category('Compression', 0x40000)
        
        f = make_feature('Float')
        f.set({'Description': 'No Description', 'DisplayPrecision': 2, 'Min': 0.01, 'Max': 1.0, 'DisplayNotation': 'Fixed', 'Value': None, 'tag': 'Float', 'Visibility': 'Beginner', 'Representation': 'Linear', 'Inc': 0.01, 'pValue': 'CompressionRatioReg', 'Unit': None, 'Name': 'CompressionRatio'})
        c.add_feature(f)
        
        f = make_feature('FloatReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RW', 'pPort': None, 'Visibility': None, 'Length': 8, 'tag': 'FloatReg', 'Value': None, 'Endianness': 'LittleEndian', 'Address': 262144, 'Name': 'CompressionRatioReg', 'pValue': None, 'Description': None})
        c.add_feature(f)
        
        f = make_feature('Enumeration')
        f.set({'Description': 'No Description', 'Value': None, 'enumEntries': [{'Name': 'Static', 'Value': 0}, {'Name': 'Active', 'Value': 1}], 'tag': 'Enumeration', 'Visibility': 'Beginner', 'Name': 'MeasurementBasis', 'txt': 'typedef enum {\n        MEASUREMENT_BASIS_CALIBRATE = 0,\n        MEASUREMENT_BASIS_FOCUS = 1,\n        MEASUREMENT_BASIS_RASTER = 2,\n        MEASUREMENT_BASIS_NOISELET = 4,\n        MEASUREMENT_BASIS_HADAMARD_WALSH = 5,\n        MEASUREMENT_BASIS_HADAMARD12 = 6,\n        MEASUREMENT_BASIS_SOLAR_EX_NOISELET = 100\n    } measurement_basis;\n', 'pValue': 'MeasurementBasisReg', 'c_name': 'measurement_basis'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RW', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': 'LittleEndian', 'Address': 262152, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'MeasurementBasisReg'})
        c.add_feature(f)
        
        f = make_feature('Enumeration')
        f.set({'Description': 'No Description', 'Value': None, 'enumEntries': [{'Name': 'Static', 'Value': 0}, {'Name': 'Active', 'Value': 1}], 'tag': 'Enumeration', 'Visibility': 'Beginner', 'Name': 'Reconstruction', 'txt': 'typedef enum {\n        RECONSTRUCTION_CO_SA_MP = 0,\n        RECONSTRUCTION_TVAL = 1,\n        RECONSTRUCTION_OCTAVE = 100,\n        RECONSTRUCTION_POWER_METER = 150\n    } reconstruction;\n', 'pValue': 'ReconstructionReg', 'c_name': 'reconstruction'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RW', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': 'LittleEndian', 'Address': 262156, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'ReconstructionReg'})
        c.add_feature(f)
        
        f = make_feature('Enumeration')
        f.set({'Description': 'No Description', 'Value': None, 'enumEntries': [{'Name': 'Static', 'Value': 0}, {'Name': 'Active', 'Value': 1}], 'tag': 'Enumeration', 'Visibility': 'Beginner', 'Name': 'BasisRowOrder', 'txt': 'typedef enum {\n        BASIS_ROW_ORDER_LINEAR = 0,\n        BASIS_ROW_ORDER_LFSR = 2\n    } basis_row_order;\n', 'pValue': 'BasisRowOrderReg', 'c_name': 'basis_row_order'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RW', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': 'LittleEndian', 'Address': 262164, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'BasisRowOrderReg'})
        c.add_feature(f)
        
        f = make_feature('Enumeration')
        f.set({'Description': 'No Description', 'Value': None, 'enumEntries': [{'Name': 'Static', 'Value': 0}, {'Name': 'Active', 'Value': 1}], 'tag': 'Enumeration', 'Visibility': 'Beginner', 'Name': 'SparsityBasis', 'txt': 'typedef enum {\n        SPARSITY_BASIS_IDENTITY = 0,\n        SPARSITY_BASIS_SQUARE_WAVELET = 1\n    } sparsity_basis;\n', 'pValue': 'SparsityBasisReg', 'c_name': 'sparsity_basis'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RW', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': 'LittleEndian', 'Address': 262168, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'SparsityBasisReg'})
        c.add_feature(f)
        
        f = make_feature('Enumeration')
        f.set({'Description': 'No Description', 'Value': None, 'enumEntries': [{'Name': 'Static', 'Value': 0}, {'Name': 'Active', 'Value': 1}], 'tag': 'Enumeration', 'Visibility': 'Beginner', 'Name': 'BasisDiagonal', 'txt': 'typedef enum {\n        BASIS_DIAGONAL_NONE = 0,\n        BASIS_DIAGONAL_BALANCED_CONSTANT = 1\n    } basis_diagonal;\n', 'pValue': 'BasisDiagonalReg', 'c_name': 'basis_diagonal'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': None, 'AccessMode': 'RO', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': None, 'Address': 262176, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'BasisDiagonalReg'})
        c.add_feature(f)
        
        f = make_feature('Integer')
        f.set({'Description': 'No Description', 'DisplayPrecision': None, 'Min': 0, 'Max': 1, 'DisplayNotation': None, 'Value': None, 'tag': 'Integer', 'Visibility': 'Beginner', 'Representation': 'Linear', 'Inc': 1, 'pValue': 'NegateModulationReg', 'Unit': None, 'Name': 'NegateModulation'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': None, 'AccessMode': 'RO', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': None, 'Address': 262180, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'NegateModulationReg'})
        c.add_feature(f)
        
        f = make_feature('Enumeration')
        f.set({'Description': 'No Description', 'Value': None, 'enumEntries': [{'Name': 'Static', 'Value': 0}, {'Name': 'Active', 'Value': 1}], 'tag': 'Enumeration', 'Visibility': 'Beginner', 'Name': 'BasisColumnOrder', 'txt': 'typedef enum {\n        BASIS_COLUMN_ORDER_LINEAR = 0,\n        BASIS_COLUMN_ORDER_LFSR = 1\n    } basis_column_order;\n', 'pValue': 'BasisColumnOrderReg', 'c_name': 'basis_column_order'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': None, 'AccessMode': 'RO', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': None, 'Address': 262184, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'BasisColumnOrderReg'})
        c.add_feature(f)
        
        self.categories.append(c)
                
        c = Category('AcquisitionAndTriggerControls', 0x50000)
        
        f = make_feature('Enumeration')
        f.set({'Description': 'This feature controls the acquisition mode of the device.', 'Value': None, 'enumEntries': [{'Name': 'Static', 'Value': 0}, {'Name': 'Active', 'Value': 1}], 'tag': 'Enumeration', 'Visibility': 'Beginner', 'Name': 'AcquisitionMode', 'txt': 'typedef enum {\n        ACQUISITION_MODE_CONTINUOUS = 0,\n        ACQUISITION_MODE_SINGLE_FRAME = 1\n    } acquisition_mode;\n', 'pValue': 'AcquisitionModeReg', 'c_name': 'acquisition_mode'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': 'WriteThrough', 'AccessMode': 'RW', 'pPort': 'Device', 'Signed': None, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': None, 'Address': 327680, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'AcquisitionModeReg'})
        c.add_feature(f)
        
        f = make_feature('Command')
        f.set({'Description': 'This feature starts the Acquisition of the device.', 'Value': None, 'tag': 'Command', 'CommandValue': 1, 'Visibility': 'Beginner', 'pValue': 'AcquisitionStartReg', 'Name': 'AcquisitionStart'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': 'WriteThrough', 'AccessMode': 'WO', 'pPort': 'Device', 'Signed': None, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': None, 'Address': 327684, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'AcquisitionStartReg'})
        c.add_feature(f)
        
        f = make_feature('Command')
        f.set({'Description': 'This feature stops the Acquisition of the device at the end of the current Frame.', 'Value': None, 'tag': 'Command', 'CommandValue': 1, 'Visibility': 'Beginner', 'pValue': 'AcquisitionStopReg', 'Name': 'AcquisitionStop'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': 'WriteThrough', 'AccessMode': 'WO', 'pPort': 'Device', 'Signed': None, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': None, 'Address': 327688, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'AcquisitionStopReg'})
        c.add_feature(f)
        
        f = make_feature('Enumeration')
        f.set({'Description': 'No Description', 'Value': None, 'enumEntries': [{'Name': 'Static', 'Value': 0}, {'Name': 'Active', 'Value': 1}], 'tag': 'Enumeration', 'Visibility': 'Beginner', 'Name': 'AcquisitionType', 'txt': 'typedef enum {\n        ACQUISITION_TYPE_IMAGING = 0,\n        ACQUISITION_TYPE_CALIBRATION = 1,\n        ACQUISITION_TYPE_POWER_METER = 2,\n        ACQUISITION_TYPE_FOCUS = 3,\n        ACQUISITION_TYPE_STATIC_MODULATION = 4\n    } acquisition_type;\n', 'pValue': 'AcquisitionTypeReg', 'c_name': 'acquisition_type'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RW', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': 'LittleEndian', 'Address': 327692, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'AcquisitionTypeReg'})
        c.add_feature(f)
        
        f = make_feature('Enumeration')
        f.set({'Description': 'No Description', 'Value': None, 'enumEntries': [{'Name': 'Static', 'Value': 0}, {'Name': 'Active', 'Value': 1}], 'tag': 'Enumeration', 'Visibility': 'Beginner', 'Name': 'ModulationRepetition', 'txt': 'typedef enum {\n        MODULATION_REPETITION_NONE = 0,\n        MODULATION_REPETITION_DUPLICATE_PATTERN = 1,\n        MODULATION_REPETITION_ALL_OFF_BEFORE_PATTERN = 2\n    } modulation_repetition;\n', 'pValue': 'ModulationRepetitionReg', 'c_name': 'modulation_repetition'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RW', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': 'LittleEndian', 'Address': 327696, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'ModulationRepetitionReg'})
        c.add_feature(f)
        
        f = make_feature('Enumeration')
        f.set({'Description': 'No Description', 'Value': None, 'enumEntries': [{'Name': 'Static', 'Value': 0}, {'Name': 'Active', 'Value': 1}], 'tag': 'Enumeration', 'Visibility': 'Beginner', 'Name': 'AcquisitionItemType', 'txt': 'typedef enum {\n        ACQUISITION_ITEM_TYPE_BASIS_ROW = 0,\n        ACQUISITION_ITEM_TYPE_CALIBRATION_CHECKER_BOARD = 1\n    } acquisition_item_type;\n', 'pValue': 'AcquisitionItemTypeReg', 'c_name': 'acquisition_item_type'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': None, 'AccessMode': 'RO', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': None, 'Address': 327700, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'AcquisitionItemTypeReg'})
        c.add_feature(f)
        
        f = make_feature('Enumeration')
        f.set({'Description': 'No Description', 'Value': None, 'enumEntries': [{'Name': 'Static', 'Value': 0}, {'Name': 'Active', 'Value': 1}], 'tag': 'Enumeration', 'Visibility': 'Beginner', 'Name': 'AcquisitionItem0Type', 'txt': 'typedef enum {\n        ACQUISITION_ITEM0_TYPE_BASIS_ROW = 0,\n        ACQUISITION_ITEM0_TYPE_CALIBRATION_CHECKER_BOARD = 1\n    } acquisition_item0_type;\n', 'pValue': 'AcquisitionItem0TypeReg', 'c_name': 'acquisition_item0_type'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': None, 'AccessMode': 'RO', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': None, 'Address': 327704, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'AcquisitionItem0TypeReg'})
        c.add_feature(f)
        
        f = make_feature('Integer')
        f.set({'Description': 'No Description', 'DisplayPrecision': None, 'Min': None, 'Max': None, 'DisplayNotation': None, 'Value': None, 'tag': 'Integer', 'Visibility': 'Beginner', 'Representation': None, 'Inc': None, 'pValue': 'AcquisitionItem0CountReg', 'Unit': None, 'Name': 'AcquisitionItem0Count'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': None, 'AccessMode': 'RO', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': None, 'Address': 327708, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'AcquisitionItem0CountReg'})
        c.add_feature(f)
        
        f = make_feature('Enumeration')
        f.set({'Description': 'No Description', 'Value': None, 'enumEntries': [{'Name': 'Static', 'Value': 0}, {'Name': 'Active', 'Value': 1}], 'tag': 'Enumeration', 'Visibility': 'Beginner', 'Name': 'AcquisitionItem1Type', 'txt': 'typedef enum {\n        ACQUISITION_ITEM1_TYPE_BASIS_ROW = 0,\n        ACQUISITION_ITEM1_TYPE_CALIBRATION_CHECKER_BOARD = 1\n    } acquisition_item1_type;\n', 'pValue': 'AcquisitionItem1TypeReg', 'c_name': 'acquisition_item1_type'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': None, 'AccessMode': 'RO', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': None, 'Address': 327712, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'AcquisitionItem1TypeReg'})
        c.add_feature(f)
        
        f = make_feature('Integer')
        f.set({'Description': 'No Description', 'DisplayPrecision': None, 'Min': None, 'Max': None, 'DisplayNotation': None, 'Value': None, 'tag': 'Integer', 'Visibility': 'Beginner', 'Representation': None, 'Inc': None, 'pValue': 'AcquisitionItem1CountReg', 'Unit': None, 'Name': 'AcquisitionItem1Count'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': None, 'AccessMode': 'RO', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': None, 'Address': 327716, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'AcquisitionItem1CountReg'})
        c.add_feature(f)
        
        f = make_feature('Enumeration')
        f.set({'Description': 'No Description', 'Value': None, 'enumEntries': [{'Name': 'Static', 'Value': 0}, {'Name': 'Active', 'Value': 1}], 'tag': 'Enumeration', 'Visibility': 'Beginner', 'Name': 'AcquisitionItem2Type', 'txt': 'typedef enum {\n        ACQUISITION_ITEM2_TYPE_BASIS_ROW = 0,\n        ACQUISITION_ITEM2_TYPE_CALIBRATION_CHECKER_BOARD = 1\n    } acquisition_item2_type;\n', 'pValue': 'AcquisitionItem2TypeReg', 'c_name': 'acquisition_item2_type'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': None, 'AccessMode': 'RO', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': None, 'Address': 327720, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'AcquisitionItem2TypeReg'})
        c.add_feature(f)
        
        f = make_feature('Integer')
        f.set({'Description': 'No Description', 'DisplayPrecision': None, 'Min': None, 'Max': None, 'DisplayNotation': None, 'Value': None, 'tag': 'Integer', 'Visibility': 'Beginner', 'Representation': None, 'Inc': None, 'pValue': 'AcquisitionItem2CountReg', 'Unit': None, 'Name': 'AcquisitionItem2Count'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': None, 'AccessMode': 'RO', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': None, 'Address': 327724, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'AcquisitionItem2CountReg'})
        c.add_feature(f)
        
        self.categories.append(c)
                
        c = Category('ExperimentLogging', 0x60000)
        
        f = make_feature('StringReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RW', 'pPort': None, 'Visibility': None, 'Length': 160, 'tag': 'StringReg', 'Value': None, 'Endianness': 'LittleEndian', 'Address': 393216, 'Name': 'ExperimentName', 'pValue': None, 'Description': None})
        c.add_feature(f)
        
        f = make_feature('StringReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RW', 'pPort': None, 'Visibility': None, 'Length': 16000, 'tag': 'StringReg', 'Value': None, 'Endianness': 'LittleEndian', 'Address': 393376, 'Name': 'ExperimentDescription', 'pValue': None, 'Description': None})
        c.add_feature(f)
        
        f = make_feature('StringReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RW', 'pPort': None, 'Visibility': None, 'Length': 4096, 'tag': 'StringReg', 'Value': None, 'Endianness': 'LittleEndian', 'Address': 409376, 'Name': 'FrameComment', 'pValue': None, 'Description': None})
        c.add_feature(f)
        
        self.categories.append(c)
                
        c = Category('Replay', 0x70000)
        
        f = make_feature('StringReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RW', 'pPort': None, 'Visibility': None, 'Length': 1600, 'tag': 'StringReg', 'Value': None, 'Endianness': 'LittleEndian', 'Address': 458752, 'Name': 'ReplayCSImage', 'pValue': None, 'Description': None})
        c.add_feature(f)
        
        f = make_feature('Integer')
        f.set({'Description': 'No Description', 'DisplayPrecision': None, 'Min': -1, 'Max': None, 'DisplayNotation': None, 'Value': None, 'tag': 'Integer', 'Visibility': 'Beginner', 'Representation': 'Linear', 'Inc': None, 'pValue': 'ReplayFrameReg', 'Unit': None, 'Name': 'ReplayFrame'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': None, 'AccessMode': 'RO', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': None, 'Address': 458912, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'ReplayFrameReg'})
        c.add_feature(f)
        
        self.categories.append(c)
                
        c = Category('Sampling', 0x80000)
        
        f = make_feature('Enumeration')
        f.set({'Description': 'No Description', 'Value': None, 'enumEntries': [{'Name': 'Static', 'Value': 0}, {'Name': 'Active', 'Value': 1}], 'tag': 'Enumeration', 'Visibility': 'Beginner', 'Name': 'SampleCalibration', 'txt': 'typedef enum {\n        SAMPLE_CALIBRATION_NONE_ = 0,\n        SAMPLE_CALIBRATION_CONSTANT_MEAN = 1,\n        SAMPLE_CALIBRATION_TIMING_ENCODED = 2,\n        SAMPLE_CALIBRATION_TIMING_ENCODED_NOTCH = 3\n    } sample_calibration;\n', 'pValue': 'SampleCalibrationReg', 'c_name': 'sample_calibration'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RW', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': 'LittleEndian', 'Address': 524288, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'SampleCalibrationReg'})
        c.add_feature(f)
        
        f = make_feature('Integer')
        f.set({'Description': 'No Description', 'DisplayPrecision': None, 'Min': None, 'Max': None, 'DisplayNotation': None, 'Value': None, 'tag': 'Integer', 'Visibility': 'Beginner', 'Representation': None, 'Inc': None, 'pValue': 'TriggeredSamplesPerMeasurementReg', 'Unit': None, 'Name': 'TriggeredSamplesPerMeasurement'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': None, 'AccessMode': 'RO', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': None, 'Address': 524292, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'TriggeredSamplesPerMeasurementReg'})
        c.add_feature(f)
        
        f = make_feature('Integer')
        f.set({'Description': 'No Description', 'DisplayPrecision': None, 'Min': None, 'Max': None, 'DisplayNotation': None, 'Value': None, 'tag': 'Integer', 'Visibility': 'Beginner', 'Representation': None, 'Inc': None, 'pValue': 'TriggeredSamplesToSkipReg', 'Unit': None, 'Name': 'TriggeredSamplesToSkip'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': None, 'AccessMode': 'RO', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': None, 'Address': 524296, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'TriggeredSamplesToSkipReg'})
        c.add_feature(f)
        
        f = make_feature('Integer')
        f.set({'Description': 'No Description', 'DisplayPrecision': None, 'Min': None, 'Max': None, 'DisplayNotation': None, 'Value': None, 'tag': 'Integer', 'Visibility': 'Beginner', 'Representation': 'Linear', 'Inc': None, 'pValue': 'TriggeredSamplesToIntegrateReg', 'Unit': None, 'Name': 'TriggeredSamplesToIntegrate'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': None, 'AccessMode': 'RO', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': None, 'Address': 524300, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'TriggeredSamplesToIntegrateReg'})
        c.add_feature(f)
        
        self.categories.append(c)
                
        c = Category('TVAL', 0x90000)
        
        f = make_feature('Integer')
        f.set({'Description': 'No Description', 'DisplayPrecision': None, 'Min': None, 'Max': None, 'DisplayNotation': None, 'Value': None, 'tag': 'Integer', 'Visibility': 'Beginner', 'Representation': None, 'Inc': None, 'pValue': 'TVALMaxIterationsReg', 'Unit': None, 'Name': 'TVALMaxIterations'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RW', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': 'LittleEndian', 'Address': 655360, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'TVALMaxIterationsReg'})
        c.add_feature(f)
        
        f = make_feature('Integer')
        f.set({'Description': 'No Description', 'DisplayPrecision': None, 'Min': None, 'Max': None, 'DisplayNotation': None, 'Value': None, 'tag': 'Integer', 'Visibility': 'Beginner', 'Representation': None, 'Inc': None, 'pValue': 'TVALNormReg', 'Unit': None, 'Name': 'TVALNorm'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RW', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': 'LittleEndian', 'Address': 655364, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'TVALNormReg'})
        c.add_feature(f)
        
        f = make_feature('Integer')
        f.set({'Description': 'No Description', 'DisplayPrecision': None, 'Min': None, 'Max': None, 'DisplayNotation': None, 'Value': None, 'tag': 'Integer', 'Visibility': 'Beginner', 'Representation': None, 'Inc': None, 'pValue': 'TVALGradientPenaltyReg', 'Unit': None, 'Name': 'TVALGradientPenalty'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RW', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': 'LittleEndian', 'Address': 655368, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'TVALGradientPenaltyReg'})
        c.add_feature(f)
        
        f = make_feature('Integer')
        f.set({'Description': 'No Description', 'DisplayPrecision': None, 'Min': None, 'Max': None, 'DisplayNotation': None, 'Value': None, 'tag': 'Integer', 'Visibility': 'Beginner', 'Representation': None, 'Inc': None, 'pValue': 'TVALScenePenaltyReg', 'Unit': None, 'Name': 'TVALScenePenalty'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RW', 'pPort': None, 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': 'LittleEndian', 'Address': 655372, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'TVALScenePenaltyReg'})
        c.add_feature(f)
        
        f = make_feature('Float')
        f.set({'Description': 'No Description', 'DisplayPrecision': None, 'Min': None, 'Max': None, 'DisplayNotation': None, 'Value': None, 'tag': 'Float', 'Visibility': 'Beginner', 'Representation': None, 'Inc': None, 'pValue': 'TVALDeltaToleranceReg', 'Unit': None, 'Name': 'TVALDeltaTolerance'})
        c.add_feature(f)
        
        f = make_feature('FloatReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RW', 'pPort': None, 'Visibility': None, 'Length': 8, 'tag': 'FloatReg', 'Value': None, 'Endianness': 'LittleEndian', 'Address': 655376, 'Name': 'TVALDeltaToleranceReg', 'pValue': None, 'Description': None})
        c.add_feature(f)
        
        f = make_feature('Float')
        f.set({'Description': 'No Description', 'DisplayPrecision': None, 'Min': None, 'Max': None, 'DisplayNotation': 'Fixed', 'Value': None, 'tag': 'Float', 'Visibility': 'Beginner', 'Representation': 'Linear', 'Inc': None, 'pValue': 'TVALFidelityToleranceReg', 'Unit': None, 'Name': 'TVALFidelityTolerance'})
        c.add_feature(f)
        
        f = make_feature('FloatReg')
        f.set({'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RW', 'pPort': None, 'Visibility': None, 'Length': 8, 'tag': 'FloatReg', 'Value': None, 'Endianness': 'LittleEndian', 'Address': 655384, 'Name': 'TVALFidelityToleranceReg', 'pValue': None, 'Description': None})
        c.add_feature(f)
        
        self.categories.append(c)
                
        c = Category('PowerMeter', 0xb0000)
        
        f = make_feature('Integer')
        f.set({'viewJsonUrl': '/feature/inview_4001.xml/PowerMeter/PowerMeterSampleCount', 'Description': 'No Description', 'DisplayPrecision': None, 'Min': None, 'Max': None, 'DisplayNotation': None, 'Value': None, 'tag': 'Integer', 'Visibility': 'Beginner', 'Representation': None, 'Inc': None, 'pValue': 'PowerMeterSampleCountReg', 'Unit': None, 'Name': 'PowerMeterSampleCount'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'viewJsonUrl': '/feature/inview_4001.xml/PowerMeter/PowerMeterSampleCountReg', 'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RW', 'pPort': '', 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': 'LittleEndian', 'Address': 720896, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'PowerMeterSampleCountReg'})
        c.add_feature(f)
        
        f = make_feature('Enumeration')
        f.set({'viewJsonUrl': '/feature/inview_4001.xml/PowerMeter/PowerMeterModulation', 'Description': 'No Description', 'Value': None, 'enumEntries': [{'Name': 'Static', 'Value': 0}, {'Name': 'Active', 'Value': 1}], 'tag': 'Enumeration', 'Visibility': 'Beginner', 'Name': 'PowerMeterModulation', 'txt': 'typedef enum {\n        POWER_METER_MODULATION_STATIC = 0,\n        POWER_METER_MODULATION_ACTIVE = 1\n    } power_meter_modulation;\n', 'pValue': 'PowerMeterModulationReg', 'c_name': 'power_meter_modulation'})
        c.add_feature(f)
        
        f = make_feature('IntReg')
        f.set({'viewJsonUrl': '/feature/inview_4001.xml/PowerMeter/PowerMeterModulationReg', 'PollingTime': None, 'Cachable': 'NoCache', 'AccessMode': 'RW', 'pPort': '', 'Signed': False, 'Length': 4, 'tag': 'IntReg', 'Visibility': None, 'Endianness': 'LittleEndian', 'Address': 720900, 'Description': None, 'Value': None, 'pValue': None, 'Name': 'PowerMeterModulationReg'})
        c.add_feature(f)
        
        self.categories.append(c)
        
                            
        print "Root successfully initialized! Categories:"
        for c in self.categories:
            print c
            
    def get_feature(self, name):
        for c in self.categories:
            for p in c.features:
                if re.compile(name).search(p.Name) is not None:
                    return p
        else:
            raise Exception('node {} not found. did it come before feature node in the xml?'.format(name)) 
                    
    def add_feature(self, cat, feat, type):
        print "adding {} feat+reg {} to {}".format(type, feat, cat)
        featRegType = {}
        featRegType['Integer'] = IntReg
        featRegType['Enumeration'] = IntReg
        featRegType['Command'] = IntReg
        featRegType['Float'] = FloatReg
        category = None
        for c in self.categories:
            if c.Name == cat:
                print "found cat {}".format(c.Name)
                category = c
                return
        else:
            raise Exception('BadCategory', cat)
        if type == 'String':
            f = make_feature('StringReg')
            f.Name = feat
            f.Address = category.regAddr
            category.regAddr += f.Length
            category.features.append(f)
        else:
            f = make_feature(type)
            f.Name = feat
            r = featRegType[type]()
            r.Name = feat+'Reg'
            r.Address = category.regAddr
            category.regAddr += r.Length
            f.pValue = r.Name
            category.features.append(f)
            category.features.append(r)
        

    
  