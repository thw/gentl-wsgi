﻿#
#    Transport Layer
#
#    python wrapper to a shared library providing the GenTL 1.2 interface.
#    Only create one of these! 
#
import sys, os, copy
from ctypes import *

from GenICam import Root
import Enums
from Interface import Interface
from Device import Device

from TransportLayerEnums import TLInfoCommand
        
class TransportLayer(object):
    '''
    provides access to the GC_____ functions defined by GenTL.
    the constructor of this class calls GCInitLib, which must only
    be called once per process. We currently have no way of ensuring
    this doesn't happen currently. Don't allocate more than one TransportLayer.
    '''            
    def __init__(self, shared_lib):
        '''
        @param shared_lib: some form of c_types shared lib (CDLL, WinDLL, etc)
        We never call TLClose or GCClose because python doesnt have 
        destructors and our TL is a global anyway that will never get deleted
        '''
        self.slib = shared_lib
        self._check(self.slib.GCInitLib())
        self.tl_id = c_int(0)
        self._check(self.slib.TLOpen(byref(self.tl_id)))
        
        self.interfaces = []
        
        self.id = self.get_info(TLInfoCommand.ID)
        self.vendor = self.get_info(TLInfoCommand.VENDOR)
        self.model = self.get_info(TLInfoCommand.MODEL)
        self.version = self.get_info(TLInfoCommand.VERSION)
        self.tl_type = self.get_info(TLInfoCommand.TLTYPE)
        self.name = self.get_info(TLInfoCommand.NAME)
        self.path_name = self.get_info(TLInfoCommand.PATHNAME)
        self.display_name = self.get_info(TLInfoCommand.DISPLAYNAME)
        
    def get_info(self, info_cmd):
        return self._get_info_buf(lambda t,b,s: self.slib.TLGetInfo(self.tl_id, c_int(info_cmd), t, b, s))
        
    def update_interface_list(self, timeout):
        '''
        update interface list with timeout 
        timeout 0xFFFFFFFFFFFFFFFF -> no timeout
        '''
        changed = c_byte(0)
        self._check(self.slib.TLUpdateInterfaceList(self.tl_id, byref(changed), c_ulonglong(0)))
        
        if changed: 
            n_ifs = c_int(0)
            self._check(self.slib.TLGetNumInterfaces(self.tl_id, byref(n_ifs)))
            
            # scan through new calls to GetInterfaceID, and match with currently stored
            # interfaces
            updated_ids = [self._get_string(lambda buf,sz: 
                            self.slib.TLGetInterfaceID(self.tl_id, c_int(ifs_index), buf, sz))
                    for ifs_index in range(n_ifs.value)]
            current_ids = [iface.id for iface in self.interfaces]
            
            to_add = [id for id in updated_ids if id not in current_ids]
            to_remove = [id for id in current_ids if id not in updated_ids]
            
            for add_id in to_add:
                self.interfaces.append(Interface(self, add_id))
            for rm_id in to_remove:
                self.interfaces = [iface for iface in self.interfaces if iface != rm_id]
    
    def get_buffer_info(self, ds_handle, buf_handle, info_flag):
        ''' DSGetBufferInfo '''
        return self._get_info_buf(lambda t,b,s: self.slib.DSGetBufferInfo(ds_handle, buf_handle, info_flag, t, b, s))
        
    def _check(self, dll_result):            
        '''
        if a dll call fails, get the last error
        '''
        if dll_result != 0:
            str_size = c_int(4096)
            err_text = create_string_buffer(str_size.value)
            err = c_int(0)
            self.slib.GCGetLastError(byref(err), err_text, byref(str_size))
            raise Exception(dll_result, err_text.value)
    
    def _get_info_buf(self, info_fn):
        '''
        provide a shortcut to allocating a space for a buffer
        info_fn is a callable
        '''
        buf_types = {            
            # INFO_DATATYPE_UNKNOWN     = 0,        /* Unknown data type */   
            # INFO_DATATYPE_STRING      = 1,        /* 0-terminated C string (ASCII encoded). */         
            1: {'c_storage': lambda buf_size: create_string_buffer(buf_size.value), 
                'c_param': lambda buf: buf,
                'py_type': lambda buf: str(buf.value)},
            # INFO_DATATYPE_STRINGLIST  = 2,        /* Concatenated INFO_DATATYPE_STRING list. End of list is signaled with an additional 0. */         
            # INFO_DATATYPE_INT16       = 3,        /* Signed 16 bit integer. */        
            # INFO_DATATYPE_UINT16      = 4,        /* unsigned 16 bit integer */       
            # INFO_DATATYPE_INT32       = 5,        /* signed 32 bit integer */         
            # INFO_DATATYPE_UINT32      = 6,        /* unsigned 32 bit integer */         
            6: {'c_storage': lambda buf_size: c_int(0), 
                'c_param': lambda buf: byref(buf),
                'py_type': lambda buf: int(buf.value)},      
            # INFO_DATATYPE_INT64       = 7,        /* signed 64 bit integer */         
            # INFO_DATATYPE_UINT64      = 8,        /* unsigned 64 bit integer */         
            8: {'c_storage': lambda buf_size: c_ulonglong(0), 
                'c_param': lambda buf: byref(buf),
                'py_type': lambda buf: long(buf.value)},
            # INFO_DATATYPE_FLOAT64     = 9,        /* Signed 64 bit floating point number. */         
            9: {'c_storage': lambda buf_size: c_double(0), 
                'c_param': lambda buf: byref(buf),
                'py_type': lambda buf: float(buf.value)},
            # INFO_DATATYPE_PTR         = 10,       /* Pointer type (void*). Size is platform dependent (32 bit on 32 bit platforms). */         
            10: {'c_storage': lambda buf_size: c_void_p(0),
                 'c_param': lambda buf: byref(buf),
                 'py_type': lambda buf: buf},
            # INFO_DATATYPE_BOOL8       = 11,       /* Boolean value occupying 8 bit. 0 for false and anything for true. */         
            # INFO_DATATYPE_SIZET       = 12,       /* Platform dependent unsigned integer (32 bit on 32 bit platforms). */         
            12: {'c_storage': lambda buf_size: c_int(0), 
                'c_param': lambda buf: byref(buf),
                'py_type': lambda buf: int(buf.value)},        
            # INFO_DATATYPE_BUFFER      = 13,       /* Like a INFO_DATATYPE_STRING but with arbitrary data and no 0 termination. */         
            13: {'c_storage': lambda buf_size: create_string_buffer(buf_size.value), 
                'c_param': lambda buf: buf,
                'py_type': lambda buf: buf.raw},
        }
        buf_size = c_int(0)
        buf_type = c_int(0)
        self._check(info_fn(byref(buf_type), None, byref(buf_size)))
        
        if buf_type.value not in buf_types:
            raise Exception('UnknownInfoBufferType', buf_type.value)
            
        buf = buf_types[buf_type.value]['c_storage'](buf_size)
        self._check(info_fn(byref(buf_type), buf_types[buf_type.value]['c_param'](buf), byref(buf_size)))
        
        return buf_types[buf_type.value]['py_type'](buf)
                
    def _get_string(self, string_fn):
        '''
        provide a shortcut to allocating a space for a buffer
        '''
        str_size = c_int(0)
        self._check(string_fn(None, byref(str_size)))
        str_buf = create_string_buffer(str_size.value)
        self._check(string_fn(str_buf, byref(str_size)))
        return str(str_buf.value)
    
    
        
        