from wtforms import Form, TextField, BooleanField, DecimalField, IntegerField, SelectField, SubmitField, FieldList, FormField, validators
import GenICam
from GenICam import get_visibility_levels, get_access_modes, get_cache_modes

class RootOptionsForm(Form):
    hideRegisters = SelectField('Hide value registers', choices=[('Yes','Yes'),('No','No')])
 
class NoForm(Form):
    existence = BooleanField('please acknowledge that this form does not exist')
    
def make_form(feat, request, port):
    class F(Form):
        pass
        
    if feat.tag == 'Integer':
        setattr(F, feat.Name, IntegerField(feat.Name))
    elif feat.tag == 'Enumeration':
        setattr(F, feat.Name, SelectField(feat.Name, choices=[(e.Value,e.Name) for e in feat.enumEntries]))
    elif feat.tag == 'StringReg':
        setattr(F, feat.Name, TextField(feat.Name, default=feat.GetValue(port)))
    elif feat.tag == 'Command':
        setattr(F, feat.Name, SubmitField(feat.Name))
    else:
        print "NoForm for {}".format(feat.tag)
        return NoForm(request.form, feat)
    #print "form: {}".format(dir(F))
    form = F(request.form, feat)
        
    print 'form {} data: {}'.format(feat.Name, form.data)
    return form
    

    