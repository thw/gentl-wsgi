﻿#
#    Transport Layer
#
#    python wrapper to a shared library providing the GenTL 1.2 interface.
#    Only create one of these! 
#
import sys, os, copy
from ctypes import *

from GenICam import Root
from Enums import *

import datetime

class TypeCodes(object):
    I8 = 1
    U8 = 2
    I16 = 3
    U16 = 4
    I32 = 5
    U32 = 6
    I64 = 7
    U64 = 8
    FLOAT = 9
    DOUBLE = 10

class Buffer(object):
    '''
    Holds a BUFFER_HANDLE
    '''
    def __init__(self, tl, ds_handle):
        ''' open self, get contents, close self '''
        self._tl = tl
        self._handle = c_int(0)
        self.open(ds_handle)
        self.fill(ds_handle)
        self.close(ds_handle)
        
    def data_dict(self, count=None):
        d = self.data
        if count is not None:
            d = self.data[:count]
        return {'timestamp': self.timestamp, 
            'sample_format': self.sample_format,
            'data': d}
            
    def open(self, ds_handle):
        ''' grab the BUFFER_HANDLE '''
        self._tl._check(self._tl.slib.DSGetBufferID(ds_handle, c_int(0), byref(self._handle)))
        
    def close(self, ds_handle):
        ''' requeue the BUFFER_HANDLE '''
        self._tl._check(self._tl.slib.DSQueueBuffer(ds_handle, self._handle))
        
    def fill(self, ds_handle):
        ''' get the buffer data and the timestamp '''
        flag_info_base      = c_int(0) # PTR        Base address of the buffer memory.
        flag_info_size      = c_int(1) # SIZET      Size of the buffer in bytes.
        flag_info_timestamp = c_int(3) # UINT64  
        flag_info_sample_format = c_int(1001) # UINT64  
        
        buf_ptr = self._tl.get_buffer_info(ds_handle, self._handle, flag_info_base)
        buf_size = self._tl.get_buffer_info(ds_handle, self._handle, flag_info_size)
        self.sample_format = self._tl.get_buffer_info(ds_handle, self._handle, flag_info_sample_format)
        if self.sample_format == TypeCodes.DOUBLE:
            buf_double_p = cast(buf_ptr, POINTER(c_double))
            element_count = buf_size / 8
            self.data = map(float, list(buf_double_p[0:element_count]))
        else:
            self.data = string_at(buf_ptr, buf_size) 
         
        # timestamp is milliseconds from GenTL. datetime expects seconds.
        time64 = self._tl.get_buffer_info(ds_handle, self._handle, flag_info_timestamp)
        self.timestamp = datetime.datetime.fromtimestamp(time64 / 1000).strftime('%Y-%m-%d %H:%M:%S')
        
        
class Port(object):
    '''
    Implements the GenTL Port specification.
    '''
    def __init__(self, tl, dev_handle):
        self.root = Root()
        self._tl = tl
        self._handle = c_int(0)
        self.open(dev_handle)
        
    def open(self, dev_handle):        
        self._tl._check(self._tl.slib.DevGetPort(dev_handle, byref(self._handle)))        
    
    def set(self, feature, value):
        feat = self.root.get_feature(feature)
        reg64 = c_longlong(feat.Address)
        c_value = feat.c_type(value)
        size = c_int(sizeof(c_value))
        self._tl._check(self._tl.slib.GCWritePort(self._handle, reg64, feat.param_type(c_value), byref(size)))
        
    def get(self, feature):
        feat = self.root.get_feature(feature)
        reg64 = c_longlong(feat.Address)
        c_value = feat.c_type()
        size = c_int(sizeof(c_value))
        self._tl._check(self._tl.slib.GCReadPort(self._handle, reg64, feat.param_type(c_value), byref(size)))        
        return feat.py_type(c_value.value)
    

class Device(object):
    '''
    Implements the GenTL Device specification.
    Provides access to register space through self.port
    '''
    def __init__(self, tl, id, iface_handle):
        self._tl = tl
        print 'gentl Device init with id = {}'.format(id)
        self.id = id
        self._handle = c_int(0)
        self._iface_handle = iface_handle
        
    def open(self):
        print 'Device.open()'
        access_flags = c_int(4) # DEVICE_ACCESS_EXCLUSIVE
        self._tl._check(self._tl.slib.IFOpenDevice(self._iface_handle, self.id, access_flags, byref(self._handle)))
        self.port = Port(self._tl, self._handle)
        
        
        
        