jQuery(document).ready(function($) {
    
    // GLOBAL NAMESPACE AND APPLICATION
    window.App = Ember.Application.create();

	// CONTROLLERS
	App.cameraViewController = Ember.Object.create({
	    
	    // All cameras
		cameras: Ember.ArrayController.create(),
		// Set cameras, camera names, 
		// and set the current camera to the
		// first camera in array
		setCameras: function (_cameras) {
		    if (_cameras != 'undefined' && _cameras != null) {
		        this.cameras.set('content', _cameras);
		        this.setCameraNames();
		        this.initCurrentCamera();
		    } else {
		        console.info('Problem: cameras not set');
		    }
		},
		// All camera names
		cameraNames: Ember.ArrayController.create(),
		// Setup array of camera names to be used 
		// to access the cameras
		setCameraNames: function () {
		    var _cn = [];
		    for (var i = 0; i < this.cameras.get('content').length; i++) {
                var c = this.cameras.get('content')[i];
                _cn.push(c.camera_name);
            }
            this.cameraNames.set('content', _cn);
            // console.log("Camera Names: " + _cn);
        },
		selectCamera: function(_selectedName) {
		    
		    if (this.currentCamera != null) {
		        this.updateCameras();
		    }
		    
            for (var i = 0; i < this.cameras.get('content').length; i++) {
                if (this.cameras.get('content')[i].camera_name === _selectedName) {
                   this.setCurrentCamera(this.cameras.get('content')[i]);
                }
            }
		},
		// Set the current camera based on selected name
		setCurrentCamera: function(_selectedCamera) {
		    this.set('currentCamera', _selectedCamera);
		    this.set('currentCameraName', _selectedCamera.camera_name);
		    // Set all widgets to current camera state
		    App.saveFileWidgetViewController.setAutosavePath(this.currentCamera.autosave_dir);
		    App.saveFileWidgetViewController.setAutosave(this.currentCamera.autosave);
		    App.qualityWidgetViewController.set(this.currentCamera.quality);
		},
		// Set current camera for the first time    
		initCurrentCamera: function() {
            this.setCurrentCamera(this.cameras.get('content')[0]);
        },
        // Update cameras in array with current camera when
        // a new current camera is selected
        updateCameras: function() {
            // console.log("Current camera: " + this.currentCamera);
            // console.log("Current camera name: " + this.currentCameraName);
            // console.log("Length of cameras: " + this.cameras.get('content').length);
            for (var i = 0; i < this.cameras.get('content').length; i++) {
                if (this.cameras.get('content')[i].camera_name === this.currentCameraName) {
                    
                    //TODO: look for a cleaner solution rather than mapping one-to-one
                    this.cameras.get('content')[i].camera_id = this.currentCamera.camera_id;
                    this.cameras.get('content')[i].camera_name = this.currentCamera.camera_name;
                    this.cameras.get('content')[i].device = this.currentCamera.device;
                    this.cameras.get('content')[i].interface_id = this.currentCamera.interface_id;
                    this.cameras.get('content')[i].interface_name = this.currentCamera.interface_name;
                    this.cameras.get('content')[i].deviceAcquireProgress = this.currentCamera.deviceAcquireProgress;
                    this.cameras.get('content')[i].deviceErrorInfo = this.currentCamera.deviceErrorInfo;
                    this.cameras.get('content')[i].deviceReconstructProgress = this.currentCamera.deviceReconstructProgress;
                    this.cameras.get('content')[i].deviceState = this.currentCamera.deviceState;
                    this.cameras.get('content')[i].deviceVoltage = this.currentCamera.deviceVoltage;
                    this.cameras.get('content')[i].db = this.currentCamera.db;
                    this.cameras.get('content')[i].autosave_dir = this.currentCamera.autosave_dir;
                    this.cameras.get('content')[i].autosave = this.currentCamera.autosave;
                    this.cameras.get('content')[i].mode = this.currentCamera.mode;
                    this.cameras.get('content')[i].type = this.currentCamera.type;
                    this.cameras.get('content')[i].quality = this.currentCamera.quality;
                    this.cameras.get('content')[i].sample_count = this.currentCamera.sample_count;
                    this.cameras.get('content')[i].lab_name = this.currentCamera.lab_name;
                }
            }
        },
		currentCamera: null,
		currentCameraName: null
	});
    
    App.captureWidgetViewController = Ember.Object.create({

	});
    
    App.saveFileWidgetViewController = Ember.Object.create({
        setAutosavePath: function(_path) {
            if (_path != 'undefined' && _path != null) {
                this.set('autosavePath', _path);
            } else {
                console.info('autosavePath not set...');
            }
        },
        setAutosave: function(_autosave) {
            if (_autosave != 'undefined' && _autosave != null) {
                this.set('autosave', _autosave);
                
                // If the camera object value autosave value differs, set it here
                if (App.cameraViewController.currentCamera.autosave != null && App.cameraViewController.currentCamera.autosave != _autosave) {
                    App.cameraViewController.currentCamera.autosave = _autosave;
                    console.log("Updating camera object: autosave ", App.cameraViewController.currentCamera.autosave);
                }
            } else {
                console.info('autosave not set...');
            }
        },
        autosave: null,
        autosavePath: null
    });
    
    App.qualityWidgetViewController = Ember.Object.create({
        quality: null,
	});
	
	App.statusBarWidgetViewController = Ember.Object.create({

	});
	
    App.meterDataWidgetViewController = Ember.Object.create({

	});

    App.sampleCountWidgetViewController = Ember.Object.create({

	});

    App.mirrorStateWidgetViewController = Ember.Object.create({

	});

    App.displayModeWidgetViewController = Ember.Object.create({

	});
	
	App.addNewCameraWidgetViewController = Ember.Object.create({
		interfaceList: Ember.ArrayController.create()
	});

	App.cameraConnectionWidgetViewController = Ember.Object.create({
		openCamera: function (_camera_id) {
		  CompressViewData.openCamera(_camera_id);
		},
		cameraList: Ember.ArrayController.create()
	});
	
	App.helpWidgetViewController = Ember.Object.create({
        
        content: Ember.Object.create({
            help_sections: Ember.ArrayController.create(),
            help: function () {
                var data = CompressViewData.getHelp();
                return this.help_sections.set('content', data.help);
            }.property()
        })
    });
    
});
