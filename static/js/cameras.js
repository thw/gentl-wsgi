jQuery(document).ready(function($) {

    Cameras = [];
    Camera = Ember.Object.extend({
        'camera_id'                 : '',
        'camera_name'               : '',
        'device'                    : '',
        'interface_id'              : '',
        'interface_name'            : '',
        'deviceAcquireProgress'     : 0,
        'deviceErrorInfo'           : '',
        'deviceReconstructProgress' : 0,
        'deviceState'               : 0,
        'deviceVoltage'             : 0,
        'db'                        : null,
        'autosave_dir'              : '',
        'autosave'                  : 0,
        'mode'                      : '',
        'type'                      : '',
        'quality'                   : 0,
        'sample_count'              : '',
        'lab_name'                  : ''
    });

    /*
     * 1) Start by scanning for connected cameras using the setInterval core JavaScript function
     * 2) Update the progress bar until the progress reaches 100
     * 3) When progress reaches 100, the call the load cameras function
     * 4) The user will be forwarded to the Home screen or the Add/remove screen
     *
     */
    function getCameras(scan) {
        CompressViewData.getCameras().success(function(data) {

            var isInitialized = data.isInitialized;
            var isCameraConnected = data.isCamerasConnected;

            if (scan && !isInitialized) {
                startScan();
            } else {
                App.addNewCameraWidgetViewController.interfaceList.set('content', data.interfaces);
                App.cameraConnectionWidgetViewController.cameraList.set('content', data.known_cameras);
                $('.splash-container').hide();
                $('.main').fadeIn();
                if (!isCameraConnected) {
                    $('#add_remove_link').click();
                } else {
                    CompressViewData.getOpenCameras().success(function(data) {

                        for (var i = 0; i < data.open_cameras.length; i++) {

                            var c = data.open_cameras[i];
                            var camera = Camera.create();

                            //camera.camera_id                      = c.camera_id;
                            //camera.camera_name                    = c.camera_name;
                            //camera.device                         = c.device;
                            //camera.interface_id                   = c.interface_id;
                            //camera.interface_name                 = c.interface_name;
                            camera.deviceAcquireProgress            = c.DeviceAcquireProgress;
                            camera.deviceErrorInfo                  = c.DeviceErrorInfo;
                            camera.deviceReconstructProgress        = c.DeviceReconstructProgress;
                            camera.deviceState                      = c.DeviceState;
                            camera.deviceVoltage                    = c.DeviceVoltage;
                            camera.db                               = c.db;
                            camera.autosave_dir                     = c.dir;
                            camera.autosave                         = c.autosave;
                            //camera.mode                           = c.mode;
                            //camera.type                           = c.type;
                            camera.quality                          = c.quality;
                            //camera.sample_count                     = c.sample_count;
                            //camera.lab_name                       = c.lab_name;

                            Cameras.push(camera);
                        }
                        
                        //  ================================================================================
                        //  = DEBUG =
                        //  ================================================================================
                        camera2 = Camera.create();
                        camera3 = Camera.create();
                        Cameras.push(camera2);
                        Cameras.push(camera3);
                        Cameras[0].camera_name  = "Lab Station 1 Camera";
                        Cameras[0].autosave     = 1;
                        Cameras[0].quality      = 10;
                        Cameras[1].camera_name  = "Lab Station 2 Camera";
                        Cameras[1].autosave_dir ="c:/my/station2/lab/images";
                        Cameras[1].autosave     = 1;
                        Cameras[1].quality      = 70;
                        Cameras[2].camera_name  ="Lab Station 3 Camera";
                        Cameras[2].autosave_dir ="c:/my/station3/lab/images";
                        Cameras[2].autosave     = 0;
                        Cameras[2].quality      = 32;
                        //  ================================================================================

                        
                        App.cameraViewController.setCameras(Cameras);

                        //  ================================================================================
                        //  = DEBUG =
                        //  ================================================================================
                        // console.log("Camera Count: " + App.cameraViewController.cameras.content.length);
                        // console.log("Cameras Info");
                        // for (var i = 0; i < App.cameraViewController.cameras.content.length; i++) {
                            // var c = App.cameraViewController.cameras.content[i];
                            // console.log("camera.camera_id                    = " + c.camera_id);
                            // console.log("camera.camera_name                  = " + c.camera_name);
                            // console.log("camera.device                       = " + c.device);
                            // console.log("camera.interface_id                 = " + c.interface_id);
                            // console.log("camera.interface_name               = " + c.interface_name);
                            // console.log("camera.deviceAcquireProgress        = " + c.deviceAcquireProgress);
                            // console.log("camera.deviceErrorInfo              = " + c.deviceErrorInfo);
                            // console.log("camera.deviceReconstructProgress    = " + c.deviceReconstructProgress);
                            // console.log("camera.deviceState                  = " + c.deviceState);
                            // console.log("camera.deviceVoltage                = " + c.deviceVoltage);
                            // console.log("camera.db                           = " + c.db);
                            // console.log("camera.autosave_dir                 = " + c.autosave_dir);
                            // console.log("camera.mode                         = " + c.mode);
                            // console.log("camera.type                         = " + c.type);
                            // console.log("camera.quality                      = " + c.quality);
                            // console.log("camera.sample_count                 = " + c.sample_count);
                            // console.log("camera.lab_name                     = " + c.lab_name);
                        // }
                        //  ================================================================================

                    });
                }
            }
        });
    }

    /***********************************************************************
     * Progress Bars
     ***********************************************************************/

    var startScan = function() {
        var timeout = 3 * 1000;
        // 30 seconds
        var progress_bar_id = '#splash_progress_bar > div.bar';
        var progress_label_id = '#splash_progress_label';
        $(progress_bar_id).css({
            width : "0%"
        });

        CompressViewData.deviceScan(timeout).success(function(data) {

            $(progress_bar_id).animate({
                width : "100%"
            }, 3 * timeout);
            setTimeout(function() {
                getCameras(false);
            }, timeout);
        });
    }
    var firstGetCameras = function() {
        getCameras(true);
    };
    setTimeout(firstGetCameras, 1);
});
