jQuery(document).ready(function($) {

    var onError500 = function(jqXHR, textStatus, errorThrown) {
        if (jqXHR.status == 500) {
            document.open();
            document.write(jqXHR.responseText);
            document.close();
        }
    }
    
    // Servciess
    CompressViewData = {

        deviceScan : function(_timeout) {
            var jqxhr = $.post('/device/scan', {
                timeout : _timeout
            }).error(onError500);
            return jqxhr;
        },

        getInterfaceList : function() {
            var jqxhr = $.get('/interfaces').error(onError500);
            return jqxhr;
        },
        getOpenCameras : function() {
            var jqxhr = $.get('/open_cameras').error(onError500);
            return jqxhr;
        },
        getKnownCameras : function() {
            var jqxhr = $.get('/known_cameras').error(onError500);
            return jqxhr;
        },

        openCamera : function(_camera_id) {
            var jqxhr = $.post('/camera_open', {
                camera_id : _camera_id
            }).error(onError500);
            return jqxhr;
        },

        startCamera : function(_camera_id, _mode, _type, _quality, _sample_count) {
            var jqxhr = $.post('/camera_start', {
                camera_id : _camera_id,
                mode : _mode,
                type : _type,
                quality : _quality,
                sample_count : _sample_count
            }).error(onError500);
            return jqxhr;
        },

        stopCamera : function(_camera_id) {
            var jqxhr = $.post('/camera_stop', {
                camera_id : _camera_id
            }).error(onError500);
            return jqxhr;
        },

        getCameras : function() {
            var jqxhr = $.get('/camera_json').error(onError500);
            return jqxhr;
        },

        getHelp : function() {

            var response = $.ajax({
                type : "GET",
                url : '/help/json',
                async : false
            }).responseText;

            return $.parseJSON(response);
        }
    };
});
