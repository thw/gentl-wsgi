jQuery(document).ready(function($) {

	// GLOBALS
	CompressViewLocation = {
		SPLASH : "/",
		HOME : "camera",
		ADD_REMOVE : "camera/list",
		HELP : "help"
	};

	loadScreen = function(_screen) {
		$('.container').fadeOut(1000, function() {
			document.location = _screen;
		});
	}

	showScreen = function() {
		$('.container').fadeIn();
	}
	
	/***********************************************************************
	 * Screen Navigation
	 **********************************************************************/
	$('#camera_link').click(function() {
		
		$('#camera').fadeIn();
		$(this).parent().addClass('active');
		
		$('#add_remove_camera').hide();
		$('#add_remove_link').parent().removeClass('active');
		
		$('#help').hide();
		$('#help_link').parent().removeClass('active');
	});
	
	$('#add_remove_link').click(function() {
		
		$('#camera').hide();
		$('#camera_link').parent().removeClass('active');
		
		$('#add_remove_camera').fadeIn();
		$(this).parent().addClass('active');
		
		$('#help').hide();
		$('#help_link').parent().removeClass('active');
	});
	
	$('#help_link').click(function() {
			
		$('#camera').hide();
		$('#camera_link').parent().removeClass('active');
		
		$('#add_remove_camera').hide();
		$('#add_remove_link').parent().removeClass('active');
		
		$('#help').fadeIn();
		$(this).parent().addClass('active');
	});
	
	/***********************************************************************
	 * Camera Navigation
	 **********************************************************************/
	$('#imaging_nav_link').click(function() {
		$(this).parent().addClass('active');
		$('#power_meter_nav_link').parent().removeClass('active');
		$('#expert_config_nav_link').parent().removeClass('active');

		$('#imaging').fadeIn();
		$('#power_meter').hide();
		$('#expert_config').hide();
	});

	$('#power_meter_nav_link').click(function() {
		$('#imaging_nav_link').parent().removeClass('active');
		$(this).parent().addClass('active');
		$('#expert_config_nav_link').parent().removeClass('active');

		$('#imaging').hide();
		$('#power_meter').fadeIn();
		$('#expert_config').hide();
	});

	$('#expert_config_nav_link').click(function() {
		$('#imaging_nav_link').parent().removeClass('active');
		$('#power_meter_nav_link').parent().removeClass('active');
		$(this).parent().addClass('active');

		$('#imaging').hide();
		$('#power_meter').hide();
		$('#expert_config').fadeIn();
	});
	
	/***********************************************************************
	 * Expand View Navigation
	 **********************************************************************/
	$('#expand_return_button').click(function() {
		$('#expand_navigation').hide();
		$('#camera_navigation').fadeIn();
		$('.expand-view').slideUp();
		$('#imaging > .left-column').delay('500').fadeIn();
		$('#imaging > .right-column').delay('500').slideDown();
	});
	
});
