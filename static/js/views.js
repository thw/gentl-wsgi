jQuery(document).ready(function($) {

    // VIEWS
    App.CameraView = Ember.View.extend({
        templateName : 'cameraView',
        contentBinding : 'App.cameraViewController',
        selectCamera: function(event) {
            App.cameraViewController.selectCamera(event.context);
            
            // When selecting a different camera we need to make all of the 
            // appropriate forntend state changes here
            
            // Change the autosave buttons to match the camera object data
            var value = App.saveFileWidgetViewController.autosave;
            if (value > 0) {
               $('#autosave_on').click();
            } else {
                $('#autosave_off').click();
            }
        },
        didInsertElement : function() {
            
        }
    });

    /***********************************************************************
     * Capture Widget
     **********************************************************************/
    App.CaptureWidgetView = Ember.View.extend({
        templateName : 'captureWidgetView',
        contentBinding : 'App.captureWidgetViewController.content',
        didInsertElement : function() {

            //console.log("Camera Name: " + App.cameraViewController.currentCamera.camera_id);

            var $record = $('#record_button');
            var $snapshot = $('#snapshot_button');
            var $stop = $('#stop_button');

            // Stores current timer
            var timer = null;

            // Time between each call to a function
            var interval = 1000;

            // Arbitrary number of units to update the progress bar per interval call
            var units = 100;

            // Total number of units; current state of the progress bar
            var progress = 0;

            // Used as a flag for all image capture buttons
            var isActive = false;
            
            // Determine the type of recording
            var isSnapshot = false;

            $record.click(function() {
                isActive = (isActive == false) ? true : false;
                isSnapshot = false;
                if (isActive == true) {
                    startBlinkingIndicator();
                    $(this).attr('disabled', 'disabled');
                    $snapshot.attr('disabled', 'disabled');

                    // TODO: Add call from flask
                    timer = setInterval(updateProgress, interval, ["capture_widget_progress_bar", units]);
                }
            });

            $snapshot.click(function() {
                isActive = (isActive == false) ? true : false;
                isSnapshot = true;
                if (isActive == true) {
                    $(this).attr('disabled', 'disabled');
                    $record.attr('disabled', 'disabled');

                    // TODO: Add call from flask
                    timer = setInterval(updateProgress, interval, ["capture_widget_progress_bar", units]);
                }
            });

            $stop.click(function() {
                isActive = (isActive == false) ? true : false;
                stopBlinkingIndicator();
                $record.removeAttr('disabled');
                $snapshot.removeAttr('disabled');
                resetProgressBar("capture_widget_progress_bar");
            });

            function startBlinkingIndicator() {
                $('#record_indicator').css('background-color', '#ff0000');
                $('#record_indicator').animate({
                    opacity : "toggle"
                }, 500, function() {
                    if (isActive == true) {
                        startBlinkingIndicator();
                    } else {
                        stopBlinkingIndicator();
                    }
                });
            }

            function stopBlinkingIndicator() {
                $('#record_indicator').css('background-color', '#660000');
                $('#record_indicator').fadeIn();
            }
            
            function resetProgressBar(_id) {
                var progress_bar_id = '#' + _id + ' > div.bar';
                clearInterval(timer);
                progress = 0;
                $(progress_bar_id).css('width', '0%');
            }

            function updateProgress(_vals) {
                var MAX = 100;
                var progress_bar_id = '#' + _vals[0] + ' > div.bar';
                if (progress < MAX) {
                    progress += _vals[1];
                    $(progress_bar_id).css('width', progress + '%');
                } else if (progress >= MAX) {
                    progress = 0;
                    if (isSnapshot) {
                        $stop.click();
                    } else {
                        $(progress_bar_id).css('width', progress + '%');
                    }
                }
            }

        }
    });

    /***********************************************************************
     * Save File Widget
     **********************************************************************/
    App.SaveFileWidgetView = Ember.View.extend({
        templateName : 'saveFileWidgetView',
        contentBinding : 'App.saveFileWidgetViewController',
        didInsertElement : function() {
            
            var $saveButton = $('#modal_image_save_button');
            var $saveFileTooltip = $('#save_file_widget_tootlip');
            
            $saveButton.click(function() {
                // TODO: Add functionality...
            });

            // Tooltip
            $saveFileTooltip.popover({
                animation : true,
                placement : 'right',
                title : 'Save and Autosave',
                content : 'Content goes here...'
            });

            // TODO: Capture autosave value upon page load; currently
            // a null value when page loads - will resolve
        },
        toggleAutosave: function(_value) { 
            // If the current camera value differs from the selected toggle
            // value, then update the camera object through the controller
            if (this.content.autosave != _value) {
                App.saveFileWidgetViewController.setAutosave(_value);
            }
        },
        setAutosaveOn: function(event) {
            console.log("setAutosaveOn set state to 'On'");
            this.toggleAutosave(1);
        },
        setAutosaveOff: function(event) {
            console.log("setAutosaveOff set state to 'Off'");
            this.toggleAutosave(0);
        }
    });

    /***********************************************************************
     * Quality Widget
     **********************************************************************/
    App.QualityWidgetView = Ember.View.extend({
        templateName : 'qualityWidgetView',
        contentBinding : 'App.qualityWidgetViewController',
        didInsertElement : function() {
            
            // Slider
            $('#amount_of_time_slider').slider({
                range : "min",
                value : 0,
                min : 1,
                max : 100,
                slide : function(event, ui) {
                    $('#amount_of_time').val(ui.value / 5);
                }
            });
            // Update UI on load
            $('#amount_of_time').val(($('#amount_of_time_slider').slider("value") / 5));

            // Tooltip
            $('#quality_widget_tooltip').popover({
                animation : true,
                placement : 'right',
                title : 'Image Qaulity',
                content : 'Content goes here...'
            });
        },
        setQualitySlider: function () {
            var qualityValue = App.qualityWidgetViewController.quality;
            $('#amount_of_time_slider').slider("value", qualityValue);
            console.log("Quality: ", qualityValue);
        }
    });

    /***********************************************************************
     * Status Bar Widget
     **********************************************************************/
    App.StatusBarWidgetView = Ember.View.extend({
        templateName : 'statusBarWidgetView',
        contentBinding : 'App.statusBarWidgetViewController.content',
        didInsertElement : function() {

            $('#expand_button').click(function() {
                $('#camera_navigation').hide();
                $('#imaging > .left-column').fadeOut();
                $('#imaging > .right-column').slideUp();
                $('#expand_navigation').fadeIn();
                $('.expand-view').delay('500').slideDown();
            });
        }
    });

    /***********************************************************************
     * Meter Data Widget
     **********************************************************************/
    App.MeterDataWidgetView = Ember.View.extend({
        templateName : 'meterDataWidgetView',
        contentBinding : 'App.meterDataWidgetViewController.content',
        didInsertElement : function() {

            // Stores current timer
            var timer = null;

            // Time between each call to a function
            var interval = 1000;

            // Arbitrary number of units to update the progress bar per interval call
            var units = 5;

            // Total number of units; current state of the progress bar
            var progress = 0;

            $('#acquire_data_button').click(function() {
                $(this).attr('disabled', 'disabled');
                timer = setInterval(updateProgressBar, interval, ["meter_data_widget_progress_bar", units]);
            });

            $('#stop_data_button').click(function() {
                $('#acquire_data_button').removeAttr('disabled');
                resetProgressBar("meter_data_widget_progress_bar");
            });

            function updateProgressBar(_vals) {
                var MAX = 100;
                var progress_bar_id = '#' + _vals[0] + ' > div.bar';

                if (progress < 100) {
                    progress += _vals[1];
                    $(progress_bar_id).css('width', progress + '%');
                } else if (progress >= MAX) {
                    progress = 0;
                    // Reset progress
                    $(progress_bar_id).css('width', progress + '%');
                    // Reset progress bar on screen
                }
            }

            function resetProgressBar(_id) {
                var progress_bar_id = '#' + _id + ' > div.bar';
                clearInterval(timer);
                progress = 0;
                $(progress_bar_id).css('width', '0%');
            }


            $('#modal_data_save_button').click(function() {
                // TODO: Add functionality...
            });

            //Tooltip
            $('#meter_data_widget_tootlip').popover({
                animation : true,
                placement : 'right',
                title : 'Acquiring Meter Data',
                content : 'Content goes here...'
            });
        }
    });

    /***********************************************************************
     * Sample Count Widget
     **********************************************************************/
    App.SampleCountWidgetView = Ember.View.extend({
        templateName : 'sampleCountWidgetView',
        contentBinding : 'App.sampleCountWidgetViewController.content',
        didInsertElement : function() {

            $('#sample_count_slider').slider({
                range : "min",
                value : 256,
                min : 0,
                max : 4096,
                step : 128,
                slide : function(event, ui) {
                    $('#sample_count_units').val(ui.value);
                }
            });
            $('#sample_count_units').val($('#sample_count_slider').slider("value"));

            //Tooltip
            $('#sample_count_widget_tooltip').popover({
                animation : true,
                placement : 'right',
                title : 'Sample Count',
                content : 'Content goes here...'
            });
        }
    });

    /***********************************************************************
     * Mirror State Widget
     **********************************************************************/
    App.MirrorStateWidgetView = Ember.View.extend({
        templateName : 'mirrorStateWidgetView',
        contentBinding : 'App.mirrorStateWidgetViewController.content',
        didInsertElement : function() {

            // Tooltip
            $('#mirror_state_widget_tooltip').popover({
                animation : true,
                placement : 'right',
                title : 'Mirror State',
                content : 'Content goes here...'
            });
        }
    });

    /***********************************************************************
     * Display Mode Widget
     **********************************************************************/
    App.DisplayModeWidgetView = Ember.View.extend({
        templateName : 'displayModeWidgetView',
        contentBinding : 'App.displayModeWidgetViewController.content',
        didInsertElement : function() {

            //Tooltip
            $('#display_mode_widget_tooltip').popover({
                animation : true,
                placement : 'right',
                title : 'Display Mode',
                content : 'Content goes here...'
            });
        }
    });

    /***********************************************************************
     * Add New Camera Widget
     **********************************************************************/
    App.AddNewCameraWidgetView = Ember.View.extend({
        templateName : 'addNewCameraWidgetView',
        contentBinding : 'App.addNewCameraWidgetViewController.content',
        didInsertElement : function(element, elementId) {

            $('#camera_connection').change(function() {

                var val = $('select#camera_connection option:selected').text();
                console.log("Selected: " + val);
                if (val == 'USB') {
                    $('.ip-field').slideUp();
                    $('#camera_ip').val('');
                } else if (val == 'IP') {
                    $('.ip-field').slideDown();
                }
            });

            //Tooltip
            $('#add_camera_widget_tootlip').popover({
                animation : true,
                placement : 'right',
                title : 'Add New Camera',
                content : 'Content goes here...'
            });
        }
    });

    /***********************************************************************
     * Camera Connection Widget
     **********************************************************************/
    App.CameraListView = Ember.View.extend({
        templateName : 'cameraListView',
        contentBinding : 'App.cameraConnectionWidgetViewController',
        didInsertElement : function() {

        },
        openCamera : function(event) {
            var _camera_id = event.target.getAttribute("rel", 2);
            console.log(_camera_id);
            App.cameraConnectionWidgetViewController.openCamera(_camera_id);

            $('#camera_link').click();

            //TODO: Add support for fading out the added camera block
            // and showing the open cameras; Add support for going to the
            // camera view with the newly opened camera displayed
        }
    })

    /***********************************************************************
     * Help Widget (Navigation and Content)
     **********************************************************************/
    App.HelpWidgetView = Ember.View.extend({
        templateName : 'helpWidgetView',
        contentBinding : 'App.helpWidgetViewController.content',
        didInsertElement : function() {
        }
    })
});

// line_graph('#raw-chart', '#raw-info', 'linear', [1,2,3,4,5,6,7,8,9,0]);
// line_graph('#power-spectrum-chart', '#power-spectrum-info', 'log', [1,2,3,4,5,6,7,8,9,0]);

