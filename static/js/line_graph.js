//////////////////////////////////////////////////////////////////////
//
//  line graph
//
var line_graph = function(chart_id, info_id, y_scale, data_array) {
    $(chart_id).html('');
    var w = 1024;
    var h = 300;
    var margin = 30;
    var margin_left = 50;
    var margin_top = 5;
    var margin_bottom = 30;
    var margin_right = 5;
    var max = d3.max(data_array) * 1.05;
    var min = d3.min(data_array);
    
    var x = d3.scale.linear().domain([0, data_array.length - 1]).range([0 + margin_left, w - margin_right]);
    var y = d3.scale.linear();
    var yTickCount = 5;
    if(y_scale == 'log') {
        y = d3.scale.log();
        yTickCount = 0;
    }
    y.domain([min, max]).range([h - margin_bottom, margin_top]);
    
    var yTicks = y.ticks(yTickCount)
    var yLabels = yTicks
    if(y_scale == 'log') {
        yLabels = [];    
        for(var s = 0; s < yTicks.length; s += 11) {
            yLabels.push(yTicks[s]);
        }
    }
    
    var vis = d3.select(chart_id)
                    .append('svg:svg')
                        .attr('width', w)
                        .attr('height', h);
    
    // axes and tickmarks
    vis.append('svg:line')
        .attr('x1', x(0))
        .attr('y1', y(min))
        .attr('x2', x(data_array.length - 1))
        .attr('y2', y(min))
        .attr('class', 'axis')
    vis.append('svg:line')
        .attr('x1', x(0))
        .attr('y1', y(min))
        .attr('x2', x(0))
        .attr('y2', y(max))
        .attr('class', 'axis')        
        
    vis.selectAll(".xLabel")
        .data(x.ticks(5))
        .enter().append("svg:text")
        .attr("class", "xLabel")
        .text(String)
        .attr("x", function(d) { return x(d) })
        .attr("y", h-10)
        .attr("text-anchor", "middle")

    vis.selectAll(".yLabel")
        .data(yLabels)
        .enter().append("svg:text")
        .attr("class", "yLabel")
        .text(function(d) { v = d; return '' + v.toExponential(2); })
        .attr("x", 0)
        .attr("y", function(d) { return y(d) })
        .attr("text-anchor", "right")
        .attr("dy", 3)
        
    var tick_w = 7;
    vis.selectAll(".xTicks")
        .data(x.ticks(5))
        .enter().append("svg:line")
        .attr("class", "xTicks")
        .attr("x1", function(d) { return x(d); })
        .attr("y1", y(min))
        .attr("x2", function(d) { return x(d); })
        .attr("y2", y(min) + tick_w)
	
    vis.selectAll(".yTicks")
        .data(yTicks)
        .enter().append("svg:line")
        .attr("class", "yTicks")
        .attr("x1", x(0))
        .attr("y1", function(d) { return y(d); })
        .attr("x2", x(0) - tick_w)
        .attr("y2", function(d) { return y(d); })
        
    // mouseover events
    var onMouseOver = function(e) {
        $(info_id).text('d ' + d[0] + ', ' + d.pageX);
    };
    
    var onMouseOut = function (d) {
        $(info_id).html('');
    }
    
    var onMouseMove = function(d, i) {
        mouse = d3.mouse(this)
        mx = mouse[0]
        my = mouse[1]
        cx = Math.round(x.invert(mx))
        cy = y.invert(my)
        if (cx >= 0 && cx < data_array.length) {
            $(info_id).text('[' + cx + ']: ' + data_array[cx]);
        }
    };

    // signal graph
    var line = d3.svg.line()
                .x(function(d,i) { return x(i) })
                .y(function(d) { return y(d) });
    vis.append('svg:path')
        .data([data_array])
        .attr('d', line);
    vis.on("mousemove", onMouseMove);
    
}