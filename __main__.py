##############################################################################
#
# __init__.py
#

from resources import app

import views

if __name__=='__main__':    
    #from werkzeug.serving import run_simple
    #run_simple('localhost', 4000, app, use_debugger=True, 
    #    use_reloader=True, processes=1)
    
    from tornado.wsgi import WSGIContainer
    from tornado.httpserver import HTTPServer
    from tornado.ioloop import IOLoop

    http_server = HTTPServer(WSGIContainer(app))
    http_server.listen(4000)
    IOLoop.instance().start()