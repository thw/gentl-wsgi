##############################################################################
#
# resources.py
#

# stdlib
import os
import logging
import inspect

# web frameworks
from flask import Flask, send_from_directory
from werkzeug import secure_filename

# GenICam
from GenICam import TL
    
app = Flask(__name__, static_folder='static')
app.config.from_pyfile('config.py')
log_file_handler = logging.FileHandler('py.log')
log_file_handler.setLevel(logging.DEBUG)
app.logger.addHandler(log_file_handler)

def log_request(response):
    app.logger.debug('request: {}'.format(response))
    return response
    
#app.after_request(log_request)
    
@app.errorhandler(404)  
def page_not_found(error):
    app.logger.warning('404:{}'.format(error))
    return 'This page does not exist', 404
    
@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static', 'images'),
                               'favicon.ico', mimetype='image/png')
