import os
import sys
import shutil
import re
import unittest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class TestWSGI(unittest.TestCase):
    def setUp():
        self.driver = webdriver.Chrome()
        
    def test_device_scan():
        drv = self.driver
        drv.get("http://localhost:4000/")
        
        
        display_check = '''
                var win = this.browserbot.getUserWindow();
                var disp = win.document.defaultView.getComputedStyle(
                    document.getElementById("splash_container"),
                    null)
                .getPropertyValue("display");
                return disp;'''
        while drv.execute_script(display_check) != 'none':
        
    def tearDown():
        self.driver.close()
        
if __name__ == '__main__':
    unittest.main()
