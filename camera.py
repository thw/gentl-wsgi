# stdlib
import sys
import os
import base64

# flask
from flask import url_for, render_template, session, request, redirect, jsonify

# mongo db
from pymongo import Connection
import bson

# our resources
from resources import app, TL
from GenICam.Enums import AcquisitionType, AcquisitionMode, PowerMeterModulation
from GenICam import DataStream


   
class Camera(object):
    ''' 
        contains the behavior we expect from our cameras, wrapped around GenTL 
        from a GenTL.Device, create a link to the first device on that interface 
        
        attempt to locate the autosave database and directory
        
        TODO: initialize a genicam root from xml file. 
    '''
    def __init__(self, iface, device, db_info=None):
        self.id = device.id
        self._interface = iface
        self._device = device
        
        self._auto_save = False        
        self._db = None
        if db_info is not None:
            try:
                self._db_connection = Connection(db_info.addr, db_info.port)
                self._db = self._db_connection[db_info.db_name]
                self._collection = self._db[db_info.collection]
            except:
                pass
        
            
        self._dir = None
        try:
            self._dir = os.path.expanduser('~/InView/Images')
        except:
            pass
            
    def _get_registers(self, register_keys):
        '''
        query genicam port for the set of registers, return values as a dict
        '''
        regs = {}
        for rk in register_keys:
            val = self._port.get(rk)
            if rk.endswith('Reg'):
                rk = rk[:-3]
                
            try:
                str = EnumString(rk, val)
                val = str
            except:
                pass #leave value alone if not an enum
            regs[rk] = val
            
        return regs
        
    def open(self):
        print 'Camera.open()'
        self._device.open()  
        self._port = self._device.port      
        self._ds_png64 = DataStream(TL, DataStream.PNG_BASE64, self._device._handle)
        self._ds_raw = DataStream(TL, DataStream.RAW_CH_0, self._device._handle)
        self._ds_power_spectrum = DataStream(TL, DataStream.POWERSPECTRUM_CH_0, self._device._handle)
        self._ds_bson = DataStream(TL, DataStream.CIMAGE_BSON, self._device._handle)
        
    def close(self):
        self._ds_png64.close()
        self._ds_raw.close()
        self._ds_power_spectrum.close()
        self._ds_bson.close()
        self._device.close()  
        
    def start_imaging(self, mode, quality):
        ''' 
            translate parameters into register settings, 
            begin an imaging acquisition 
        '''
        print 'Camera.start_imaging()'
        self._ds_raw.stop()
        self._ds_power_spectrum.stop()
        self._ds_png64.start()
        self._ds_bson.start()
        self._port.set('AcquisitionModeReg', mode)
        self._port.set('AcquisitionTypeReg', AcquisitionType.Imaging)
        self._port.set('ImageSizeXReg', x)
        self._port.set('ImageSizeYReg', y)
        self._port.set('CompressionRatioReg', compression)
        self._port.set('ReconstructionReg', Reconstruction.TVAL)
        self._port.set('TVALMaxIterationsReg', 5) # TODO stop testing w/ this
        self._port.set('AcquisitionStartReg', 1)
        
        
    def start_power_meter(self, mode, sample_count, modulation):
        ''' 
            translate paramters into register settings, 
            begin a power meter acquisition 
        '''
        print 'Camera.start_power_meter()'
        self._ds_raw.start()
        self._ds_power_spectrum.start()
        self._ds_png64.stop()
        self._ds_bson.stop()
        self._port.set('AcquisitionModeReg', mode)
        self._port.set('AcquisitionTypeReg', AcquisitionType.PowerMeter)
        self._port.set('ImageSizeXReg', x)
        self._port.set('ImageSizeYReg', y)
        self._port.set('PowerMeterSampleCountReg', sample_count)
        self._port.set('PowerMeterModulationReg', modulation)
        self._port.set('AcquisitionStartReg', 1)
        
    def stop(self):
        ''' 
            stop acquiring
        ''' 
        self._port.set('AcquisitionStopReg', 1)

    def info(self):
        '''
        cameras with unopened devices
        '''
        # device information
        return { 'device': self._device.id,
            'interface_id': self._interface.id,
            'interface_name': self._interface.display_name,
            }
        
    def status(self):
        '''
        called every few ms by js to update display. keep it quick.
        '''
        # status registers
        status_keys = ['DeviceVoltageReg', 'DeviceStateReg', 
            'DeviceErrorInfo', 'DeviceAcquireProgressReg',
            'DeviceReconstructProgressReg']
        st = self._get_registers(status_keys)
        
        # output info
        st['db'] = self._db
        st['dir'] = self._dir
        
        # output streams
        max_len = 20 * 1024 * 1024 # many mb objects in the dom is ridiculous
        for ds in [self._ds_png64, self._ds_raw, self._ds_power_spectrum]:
            if ds.ready_count() > 0:
                k = ds.id_key()
                buf = ds.get_buffer()
                self.recent_data.append({k: buf})          
                print "adding {} to device status".format(k)
                st[k] = buf.data_dict(count=max_len)
            
        if self._db is not None and self._ds_bson.ready_count() > 0:
            buf = self._ds_bson.get_buffer()
            bson_objs = bson.decode_all(buf.data)
            print 'got {} BSON objects'.format(len(bson_objs))
            for bo in bson_objs:
                self._collection.insert(bo)
                
        return st
            
    